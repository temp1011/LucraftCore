package lucraft.mods.lucraftcore.utilities.blocks;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.util.energy.EnergyStorageExt;
import lucraft.mods.lucraftcore.utilities.container.ContainerFurnaceGenerator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.tileentity.TileEntityLockable;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;

import javax.annotation.Nullable;

public class TileEntityFurnaceGenerator extends TileEntityLockable implements IInventory, ITickable {

	private NonNullList<ItemStack> inventory = NonNullList.<ItemStack> withSize(1, ItemStack.EMPTY);
	private String customName;
	public EnergyStorageExt energyStorage = new EnergyStorageExt(100000, 1000);
	public int fuel;
	public int totalFuel;

	@Override
	public void update() {
		//if (this.world.isRemote)
		//	return;

		ItemStack itemstack = this.inventory.get(0);

		if (this.fuel <= 0 && energyStorage.getEnergyStored() < energyStorage.getMaxEnergyStored() && !itemstack.isEmpty() && TileEntityFurnace.isItemFuel(itemstack)) {
			this.fuel = TileEntityFurnace.getItemBurnTime(itemstack);
			this.totalFuel = fuel;
			itemstack.shrink(1);
		}

		if (this.fuel > 0) {
			this.fuel--;
			energyStorage.receiveEnergy(10, false);
		}

		if (this.energyStorage.getEnergyStored() > 0) {
			for (EnumFacing facing : EnumFacing.VALUES) {
				TileEntity tileEntity = this.getWorld().getTileEntity(this.getPos().add(facing.getDirectionVec()));

				if (tileEntity != null && !(tileEntity instanceof TileEntityFurnaceGenerator) && tileEntity.hasCapability(CapabilityEnergy.ENERGY, facing.getOpposite())) {
					IEnergyStorage energyStorage = tileEntity.getCapability(CapabilityEnergy.ENERGY, facing.getOpposite());

					if (energyStorage.getEnergyStored() < energyStorage.getMaxEnergyStored()) {
						int i = 1000;
						int maxOutput = this.energyStorage.extractEnergy(i, true);
						int maxInput = energyStorage.receiveEnergy(i, true);
						int energy = Math.min(maxInput, maxOutput);
						int energy2 = Math.min(maxInput, maxOutput);

						if (energy > 0) {
							energyStorage.receiveEnergy(energy, false);
							this.energyStorage.extractEnergy(energy2, false);
						}
					}
				}
			}
		}
	}

	@Override
	public int getSizeInventory() {
		return inventory.size();
	}

	@Override
	public boolean isEmpty() {
		for (ItemStack itemstack : this.inventory) {
			if (!itemstack.isEmpty()) {
				return false;
			}
		}

		return true;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		return this.inventory.get(index);
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		return ItemStackHelper.getAndSplit(this.inventory, index, count);
	}

	@Override
	public ItemStack removeStackFromSlot(int index) {
		return ItemStackHelper.getAndRemove(this.inventory, index);
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		this.inventory.set(index, stack);
		this.markDirty();
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUsableByPlayer(EntityPlayer player) {
		if (this.world.getTileEntity(this.pos) != this) {
			return false;
		} else {
			return player.getDistanceSq((double) this.pos.getX() + 0.5D, (double) this.pos.getY() + 0.5D, (double) this.pos.getZ() + 0.5D) <= 64.0D;
		}
	}

	@Override
	public void openInventory(EntityPlayer player) {

	}

	@Override
	public void closeInventory(EntityPlayer player) {

	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		return TileEntityFurnace.isItemFuel(stack);
	}

	@Override
	public int getField(int id) {
		if (id == 0)
			return this.energyStorage.getEnergyStored();
		else if (id == 1)
			return this.fuel;
		else if(id == 2)
			return this.totalFuel;
		return 0;
	}

	@Override
	public void setField(int id, int value) {
		if (id == 0)
			this.energyStorage.setEnergyStored(value);
		else if (id == 1)
			this.fuel = value;
		else if(id == 2)
			this.totalFuel = value;
	}

	@Override
	public int getFieldCount() {
		return 3;
	}

	@Override
	public void clear() {
		this.inventory.clear();
	}

	@Override
	public Container createContainer(InventoryPlayer playerInventory, EntityPlayer playerIn) {
		return new ContainerFurnaceGenerator(playerInventory, this);
	}

	@Override
	public String getGuiID() {
		return LucraftCore.MODID + "furnace_generator";
	}

	@Override
	public String getName() {
		return this.hasCustomName() ? this.customName : "container.furnace_generator";
	}

	@Override
	public boolean hasCustomName() {
		return this.customName != null && !this.customName.isEmpty();
	}

	public void setCustomInventoryName(String name) {
		this.customName = name;
	}

	@Override
	public ITextComponent getDisplayName() {
		return hasCustomName() ? new TextComponentString(customName) : new TextComponentTranslation("tile.furnace_generator.name", new Object[0]);
	}

	@Override
	public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
		return super.hasCapability(capability, facing) || capability == CapabilityEnergy.ENERGY;
	}

	@Nullable
	@Override
	public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
		if (capability == CapabilityEnergy.ENERGY)
			return CapabilityEnergy.ENERGY.cast(this.energyStorage);
		return super.getCapability(capability, facing);
	}

	@Override
	public void readFromNBT(NBTTagCompound compound) {
		super.readFromNBT(compound);

		this.inventory = NonNullList.<ItemStack> withSize(this.getSizeInventory(), ItemStack.EMPTY);
		ItemStackHelper.loadAllItems(compound, this.inventory);
		this.energyStorage.deserializeNBT(compound);
		this.fuel = compound.getInteger("Fuel");
		this.totalFuel = compound.getInteger("TotalFuel");

		if (compound.hasKey("CustomName", 8))
			this.customName = compound.getString("CustomName");
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		super.writeToNBT(compound);

		ItemStackHelper.saveAllItems(compound, this.inventory);
		compound.setInteger("Energy", this.energyStorage.getEnergyStored());
		compound.setInteger("Fuel", this.fuel);
		compound.setInteger("TotalFuel", this.totalFuel);

		if (this.hasCustomName())
			compound.setString("CustomName", this.customName);

		return compound;
	}

	@Override
	public void markDirty() {
		super.markDirty();
		this.getWorld().notifyBlockUpdate(getPos(), getWorld().getBlockState(getPos()), getWorld().getBlockState(getPos()), 3);
	}

	@Override
	public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
		readFromNBT(pkt.getNbtCompound());
		world.markBlockRangeForRenderUpdate(getPos(), getPos());
	}

	@Override
	public SPacketUpdateTileEntity getUpdatePacket() {
		NBTTagCompound tag = new NBTTagCompound();
		writeToNBT(tag);
		return new SPacketUpdateTileEntity(getPos(), 1, tag);
	}

	@Override
	public NBTTagCompound getUpdateTag() {
		NBTTagCompound nbt = super.getUpdateTag();
		writeToNBT(nbt);
		return nbt;
	}
}
