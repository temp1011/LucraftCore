package lucraft.mods.lucraftcore.utilities.blocks;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.utilities.container.ContainerExtractor;
import lucraft.mods.lucraftcore.utilities.recipes.ExtractorRecipeHandler;
import lucraft.mods.lucraftcore.utilities.recipes.IExtractorRecipe;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.*;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.tileentity.TileEntityLockable;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fluids.*;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.FluidTankProperties;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidTankProperties;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.items.wrapper.InvWrapper;

import javax.annotation.Nullable;
import java.util.Random;

public class TileEntityExtractor extends TileEntityLockable implements ISidedInventory, ITickable {

	private static final int[] SLOTS_TOP = new int[] { 0 };
	private static final int[] SLOTS_BOTTOM = new int[] { 1, 2, 3 };
	private static final int[] SLOTS_SIDES = new int[] { 3 };
	public static final int TANK_CAPACITY = 5000;

	private NonNullList<ItemStack> inventory = NonNullList.<ItemStack> withSize(6, ItemStack.EMPTY);
	public FluidTank fluidTank = new FluidTank(TANK_CAPACITY);
	private String customName;
	private int burnTime;
	private int currentItemBurnTime;
	private int cookTime;
	private int totalCookTime;

	@Override
	public void update() {
		boolean burning = this.isBurning();
		boolean update = false;

		if (this.isBurning())
			--this.burnTime;

		if (!this.world.isRemote) {

			ItemStack fluidContainer = this.getStackInSlot(5);
			if (!fluidContainer.isEmpty() && isDrainableFilledContainer(fluidContainer) && (this.fluidTank.getFluid() == null || this.fluidTank.getFluid().isFluidEqual(FluidUtil.getFluidContained(fluidContainer)))) {
				IItemHandler inv = new InvWrapper(this);
				FluidActionResult result = FluidUtil.tryEmptyContainerAndStow(fluidContainer, this.fluidTank, inv, this.fluidTank.getCapacity(), null, true);
				if (result.isSuccess()) {
					this.setInventorySlotContents(5, result.getResult());
				}
				update = true;

			}

			ItemStack fuel = this.getStackInSlot(3);
			ItemStack input = this.getStackInSlot(0);
			ItemStack input2 = this.getStackInSlot(4);
			IExtractorRecipe recipe = ExtractorRecipeHandler.getRecipeFromInput(input, input2);

			if (this.isBurning() || !fuel.isEmpty() && !input.isEmpty()) {
				if (!this.isBurning() && canWork(recipe)) {
					this.burnTime = TileEntityFurnace.getItemBurnTime(fuel);
					this.currentItemBurnTime = this.burnTime;

					if (this.isBurning()) {
						update = true;

						if (!fuel.isEmpty()) {
							Item item = fuel.getItem();
							fuel.shrink(1);

							if (fuel.isEmpty()) {
								ItemStack item1 = item.getContainerItem(fuel);
								this.inventory.set(1, item1);
							}
						}
					}
				}

				if (this.isBurning() && canWork(recipe)) {
					++this.cookTime;

					if (this.cookTime == this.totalCookTime) {
						this.cookTime = 0;
						this.totalCookTime = this.getCookTime(input);
						this.createItems(recipe);
						update = true;
					}
				} else {
					this.cookTime = 0;
				}
			}

			else if (!this.isBurning() && this.cookTime > 0) {
				this.cookTime = MathHelper.clamp(this.cookTime - 2, 0, this.totalCookTime);
			}

			if (burning != this.isBurning())
				update = true;
		}

		if (update) {
			this.markDirty();
			this.getWorld().notifyBlockUpdate(getPos(), getWorld().getBlockState(getPos()), getWorld().getBlockState(getPos()), 3);
		}
	}

	public boolean isDrainableFilledContainer(ItemStack container) {
		IFluidHandler fluidHandler = FluidUtil.getFluidHandler(container);
		if (fluidHandler == null) {
			return false;
		}
		IFluidTankProperties[] tankProperties = fluidHandler.getTankProperties();
		for (IFluidTankProperties properties : tankProperties) {
			if (!properties.canDrain()) {
				return false;
			}
			FluidStack contents = properties.getContents();
			if (contents == null || contents.amount < properties.getCapacity()) {
				return false;
			}
		}
		return true;
	}

	public boolean canWork(IExtractorRecipe recipe) {
		if (recipe == null)
			return false;

		if (recipe.getRequiredFluid() != null && !recipe.isRequiredFluid(this.fluidTank.getFluid()))
			return false;

		ItemStack output1 = this.getStackInSlot(1);
		ItemStack output2 = this.getStackInSlot(2);
		ItemStack priOutput = recipe.getPrimaryResult();
		ItemStack secOutput = recipe.getSecondaryResult();

		boolean flag1 = output1.isEmpty() || priOutput.isEmpty() ? true : (ItemHandlerHelper.canItemStacksStack(output1, priOutput) && output1.getCount() + priOutput.getCount() <= this.getInventoryStackLimit() && output1.getCount() + priOutput.getCount() <= output1.getMaxStackSize() && output1.getCount() + priOutput.getCount() <= priOutput.getMaxStackSize());
		boolean flag2 = output2.isEmpty() || secOutput.isEmpty() ? true : (ItemHandlerHelper.canItemStacksStack(output2, secOutput) && output2.getCount() + secOutput.getCount() <= this.getInventoryStackLimit() && output2.getCount() + secOutput.getCount() <= output2.getMaxStackSize() && output2.getCount() + secOutput.getCount() <= secOutput.getMaxStackSize());

		return flag1 && flag2;
	}

	public void createItems(IExtractorRecipe recipe) {
		if (recipe != null && canWork(recipe)) {
			ItemStack input = this.getStackInSlot(0);
			ItemStack input2 = this.getStackInSlot(4);
			ItemStack output1 = recipe.getPrimaryResult().copy();
			ItemStack output2 = recipe.getSecondaryResult().copy();
			System.out.println(recipe.getPrimaryResult().getTagCompound().getString("Injection"));
            System.out.println(output1.getTagCompound().getString("Injection"));
			Random rand = new Random();
			if (!output1.isEmpty() && rand.nextFloat() <= recipe.getPrimaryChance()) {
				addResult(1, output1);
				input2.shrink(1);
			}
			if (!output2.isEmpty() && rand.nextFloat() <= recipe.getSecondaryChance())
				addResult(2, output2);

			this.fluidTank.drain(recipe.getRequiredFluid(), true);
			input.shrink(1);
		}
	}

	public int getCookTime(ItemStack stack) {
		return 200;
	}

	public boolean isBurning() {
		return this.burnTime > 0;
	}

	@SideOnly(Side.CLIENT)
	public static boolean isBurning(IInventory inventory) {
		return inventory.getField(0) > 0;
	}

	public void addResult(int index, ItemStack stack) {
		if (this.getStackInSlot(index).isEmpty())
			this.setInventorySlotContents(index, stack);
		else
			getStackInSlot(index).grow(stack.getCount());
	}

	@Override
	public void readFromNBT(NBTTagCompound compound) {
		super.readFromNBT(compound);
		this.inventory = NonNullList.<ItemStack> withSize(this.getSizeInventory(), ItemStack.EMPTY);
		ItemStackHelper.loadAllItems(compound, this.inventory);
		if (compound.hasKey("CustomName", 8))
			this.customName = compound.getString("CustomName");
		this.burnTime = compound.getInteger("BurnTime");
		this.cookTime = compound.getInteger("CookTime");
		this.totalCookTime = compound.getInteger("CookTimeTotal");
		this.currentItemBurnTime = TileEntityFurnace.getItemBurnTime(this.inventory.get(3));
		this.fluidTank.readFromNBT(compound.getCompoundTag("FluidTank"));
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		ItemStackHelper.saveAllItems(compound, inventory);
		if (this.hasCustomName())
			compound.setString("CustomName", this.customName);
		compound.setInteger("BurnTime", (short) this.burnTime);
		compound.setInteger("CookTime", (short) this.cookTime);
		compound.setInteger("CookTimeTotal", (short) this.totalCookTime);
		compound.setTag("FluidTank", this.fluidTank.writeToNBT(new NBTTagCompound()));
		return super.writeToNBT(compound);
	}

	@Override
	public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
		readFromNBT(pkt.getNbtCompound());
		world.markBlockRangeForRenderUpdate(getPos(), getPos());
	}

	@Override
	public SPacketUpdateTileEntity getUpdatePacket() {
		NBTTagCompound tag = new NBTTagCompound();
		writeToNBT(tag);
		return new SPacketUpdateTileEntity(getPos(), 1, tag);
	}

	@Override
	public NBTTagCompound getUpdateTag() {
		NBTTagCompound nbt = super.getUpdateTag();
		writeToNBT(nbt);
		return nbt;
	}

	@Override
	public int getSizeInventory() {
		return inventory.size();
	}

	@Override
	public boolean isEmpty() {
		for (ItemStack itemstack : this.inventory) {
			if (!itemstack.isEmpty()) {
				return false;
			}
		}

		return true;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		return this.inventory.get(index);
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		return ItemStackHelper.getAndSplit(this.inventory, index, count);
	}

	@Override
	public ItemStack removeStackFromSlot(int index) {
		return ItemStackHelper.getAndRemove(this.inventory, index);
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		ItemStack itemstack = this.inventory.get(index);
		boolean flag = !stack.isEmpty() && stack.isItemEqual(itemstack) && ItemStack.areItemStackTagsEqual(stack, itemstack);
		this.inventory.set(index, stack);

		if (stack.getCount() > this.getInventoryStackLimit()) {
			stack.setCount(this.getInventoryStackLimit());
		}

		if (index == 0 && !flag) {
			this.totalCookTime = this.getCookTime(stack);
			this.cookTime = 0;
			this.markDirty();
		}
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUsableByPlayer(EntityPlayer player) {
		if (this.world.getTileEntity(this.pos) != this) {
			return false;
		} else {
			return player.getDistanceSq((double) this.pos.getX() + 0.5D, (double) this.pos.getY() + 0.5D, (double) this.pos.getZ() + 0.5D) <= 64.0D;
		}
	}

	@Override
	public void openInventory(EntityPlayer player) {

	}

	@Override
	public void closeInventory(EntityPlayer player) {

	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		if (index == 1 || index == 2)
			return false;
		else if (index != 3) {
			return true;
		} else {
			ItemStack itemstack = this.inventory.get(3);
			return TileEntityFurnace.isItemFuel(stack) || SlotFurnaceFuel.isBucket(stack) && itemstack.getItem() != Items.BUCKET;
		}
	}

	@Override
	public int getField(int id) {
		switch (id) {
		case 0:
			return this.burnTime;
		case 1:
			return this.currentItemBurnTime;
		case 2:
			return this.cookTime;
		case 3:
			return this.totalCookTime;
		default:
			return 0;
		}
	}

	@Override
	public void setField(int id, int value) {
		switch (id) {
		case 0:
			this.burnTime = value;
			break;
		case 1:
			this.currentItemBurnTime = value;
			break;
		case 2:
			this.cookTime = value;
			break;
		case 3:
			this.totalCookTime = value;
		}
	}

	@Override
	public int getFieldCount() {
		return 4;
	}

	@Override
	public void clear() {
		this.inventory.clear();
	}

	@Override
	public String getName() {
		return this.hasCustomName() ? this.customName : "tile.extractor.name";
	}

	@Override
	public ITextComponent getDisplayName() {
		return hasCustomName() ? new TextComponentString(customName) : new TextComponentTranslation("tile.extractor.name", new Object[0]);
	}

	@Override
	public boolean hasCustomName() {
		return this.customName != null && !this.customName.isEmpty();
	}

	public void setCustomInventoryName(String name) {
		this.customName = name;
	}

	@Override
	public int[] getSlotsForFace(EnumFacing side) {
		if (side == EnumFacing.DOWN) {
			return SLOTS_BOTTOM;
		} else {
			return side == EnumFacing.UP ? SLOTS_TOP : SLOTS_SIDES;
		}
	}

	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction) {
		return this.isItemValidForSlot(index, itemStackIn);
	}

	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction) {
		if (direction == EnumFacing.DOWN && index == 3) {
			Item item = stack.getItem();

			if (item != Items.WATER_BUCKET && item != Items.BUCKET) {
				return false;
			}
		}

		return true;
	}

	net.minecraftforge.items.IItemHandler handlerTop = new net.minecraftforge.items.wrapper.SidedInvWrapper(this, net.minecraft.util.EnumFacing.UP);
	net.minecraftforge.items.IItemHandler handlerBottom = new net.minecraftforge.items.wrapper.SidedInvWrapper(this, net.minecraft.util.EnumFacing.DOWN);
	net.minecraftforge.items.IItemHandler handlerSide = new net.minecraftforge.items.wrapper.SidedInvWrapper(this, net.minecraft.util.EnumFacing.WEST);

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing from) {
		return super.hasCapability(capability, from) || capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY;
	}

	@SuppressWarnings("unchecked")
	@Override
	@javax.annotation.Nullable
	public <T> T getCapability(net.minecraftforge.common.capabilities.Capability<T> capability, @javax.annotation.Nullable net.minecraft.util.EnumFacing facing) {
		if (capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY) {
			return CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY.cast(new IFluidHandler() {
				@Override
				public IFluidTankProperties[] getTankProperties() {
					FluidTankInfo info = fluidTank.getInfo();
					return new IFluidTankProperties[] { new FluidTankProperties(info.fluid, info.capacity, true, true) };
				}

				@Override
				public int fill(FluidStack resource, boolean doFill) {
					int i = fluidTank.fill(resource, doFill);
					markDirty();
					getWorld().notifyBlockUpdate(getPos(), getWorld().getBlockState(getPos()), getWorld().getBlockState(getPos()), 3);
					return i;
				}

				@Nullable
				@Override
				public FluidStack drain(FluidStack resource, boolean doDrain) {
					FluidStack stack = fluidTank.drain(resource, doDrain);
					markDirty();
					getWorld().notifyBlockUpdate(getPos(), getWorld().getBlockState(getPos()), getWorld().getBlockState(getPos()), 3);
					return stack;
				}

				@Nullable
				@Override
				public FluidStack drain(int maxDrain, boolean doDrain) {
					FluidStack stack = fluidTank.drain(maxDrain, doDrain);
					markDirty();
					getWorld().notifyBlockUpdate(getPos(), getWorld().getBlockState(getPos()), getWorld().getBlockState(getPos()), 3);
					return stack;
				}
			});
		}
		if (facing != null && capability == net.minecraftforge.items.CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
			if (facing == EnumFacing.DOWN)
				return (T) handlerBottom;
			else if (facing == EnumFacing.UP)
				return (T) handlerTop;
			else
				return (T) handlerSide;
		}
		return super.getCapability(capability, facing);
	}

	@Override
	public Container createContainer(InventoryPlayer playerInventory, EntityPlayer playerIn) {
		return new ContainerExtractor(playerInventory, this);
	}

	@Override
	public String getGuiID() {
		return LucraftCore.MODID + ":extractor";
	}
}
