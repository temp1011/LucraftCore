package lucraft.mods.lucraftcore.utilities.container;

import lucraft.mods.lucraftcore.utilities.blocks.TileEntityExtractor;
import lucraft.mods.lucraftcore.utilities.recipes.ExtractorRecipeHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnaceFuel;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ContainerExtractor extends Container {

	public TileEntityExtractor tileEntity;
	private int cookTime;
	private int totalCookTime;
	private int burnTime;
	private int currentItemBurnTime;

	public ContainerExtractor(InventoryPlayer playerInventory, TileEntityExtractor tileEntity) {
		this.tileEntity = tileEntity;
		this.addSlotToContainer(new Slot(tileEntity, 0, 47, 17));
		this.addSlotToContainer(new SlotExtractorOutput(tileEntity, 1, 123, 35));
		this.addSlotToContainer(new SlotExtractorOutput(tileEntity, 2, 148, 35));
		this.addSlotToContainer(new SlotFurnaceFuel(tileEntity, 3, 47, 53));
		this.addSlotToContainer(new Slot(tileEntity, 4, 68, 35));
		this.addSlotToContainer(new Slot(tileEntity, 5, 26, 53) {
			@Override
			public int getSlotStackLimit() {
				return 1;
			}

			@Override
			public boolean isItemValid(ItemStack stack) {
				return FluidUtil.getFluidHandler(stack) != null;
			}
		});

		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 9; ++j) {
				this.addSlotToContainer(new Slot(playerInventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
			}
		}

		for (int k = 0; k < 9; ++k) {
			this.addSlotToContainer(new Slot(playerInventory, k, 8 + k * 18, 142));
		}
	}

	@Override
	public void addListener(IContainerListener listener) {
		super.addListener(listener);
		listener.sendAllWindowProperties(this, this.tileEntity);
	}

	@Override
	public void detectAndSendChanges() {
		super.detectAndSendChanges();

		for (int i = 0; i < this.listeners.size(); ++i) {
			IContainerListener icontainerlistener = this.listeners.get(i);

			if (this.cookTime != this.tileEntity.getField(2)) {
				icontainerlistener.sendWindowProperty(this, 2, this.tileEntity.getField(2));
			}

			if (this.burnTime != this.tileEntity.getField(0)) {
				icontainerlistener.sendWindowProperty(this, 0, this.tileEntity.getField(0));
			}

			if (this.currentItemBurnTime != this.tileEntity.getField(1)) {
				icontainerlistener.sendWindowProperty(this, 1, this.tileEntity.getField(1));
			}

			if (this.totalCookTime != this.tileEntity.getField(3)) {
				icontainerlistener.sendWindowProperty(this, 3, this.tileEntity.getField(3));
			}
		}

		this.cookTime = this.tileEntity.getField(2);
		this.burnTime = this.tileEntity.getField(0);
		this.currentItemBurnTime = this.tileEntity.getField(1);
		this.totalCookTime = this.tileEntity.getField(3);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void updateProgressBar(int id, int data) {
		this.tileEntity.setField(id, data);
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return this.tileEntity.isUsableByPlayer(playerIn);
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
		ItemStack itemstack = ItemStack.EMPTY;
		Slot slot = this.inventorySlots.get(index);

		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (index == 1 || index == 2) {
				if (!this.mergeItemStack(itemstack1, 6, 41, true)) {
					return ItemStack.EMPTY;
				}

				slot.onSlotChange(itemstack1, itemstack);
			} else if (index != 0 && index != 3 && index != 4 && index != 5) {
				if (ExtractorRecipeHandler.getRecipeFromFirstInput(itemstack1).size() > 0) {
					if (!this.mergeItemStack(itemstack1, 0, 1, false)) {
						return ItemStack.EMPTY;
					}
				} else if (ExtractorRecipeHandler.getRecipeFromContainerInput(itemstack1).size() > 0) {
					if (!this.mergeItemStack(itemstack1, 4, 5, false)) {
						return ItemStack.EMPTY;
					}
				} else if (TileEntityFurnace.isItemFuel(itemstack1)) {
					if (!this.mergeItemStack(itemstack1, 3, 4, false)) {
						return ItemStack.EMPTY;
					}
				} else if (index >= 6 && index < 32) {
					if (!this.mergeItemStack(itemstack1, 32, 41, false)) {
						return ItemStack.EMPTY;
					}
				} else if (index >= 32 && index < 41 && !this.mergeItemStack(itemstack1, 6, 32, false)) {
					return ItemStack.EMPTY;
				}
			} else if (!this.mergeItemStack(itemstack1, 6, 41, false)) {
				return ItemStack.EMPTY;
			}

			if (itemstack1.isEmpty()) {
				slot.putStack(ItemStack.EMPTY);
			} else {
				slot.onSlotChanged();
			}

			if (itemstack1.getCount() == itemstack.getCount()) {
				return ItemStack.EMPTY;
			}

			slot.onTake(playerIn, itemstack1);
		}

		return itemstack;
	}
}
