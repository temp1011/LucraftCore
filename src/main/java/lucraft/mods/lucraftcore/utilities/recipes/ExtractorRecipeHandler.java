package lucraft.mods.lucraftcore.utilities.recipes;

import com.google.common.collect.ImmutableList;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExtractorRecipeHandler {

	private static List<IExtractorRecipe> RECIPES = new ArrayList<>();

	public static void addRecipe(IExtractorRecipe recipe) {
		RECIPES.add(recipe);
	}

	public static List<IExtractorRecipe> getRecipes() {
		return ImmutableList.copyOf(RECIPES);
	}

	public static IExtractorRecipe getRecipeFromInput(ItemStack input, ItemStack inputContainer) {
	    if(input.isEmpty())
	        return null;
		for (IExtractorRecipe recipes : getRecipes()) {
			if (recipes.isInput(input) && recipes.isInputContainer(inputContainer)) {
				return recipes;
			}
		}
		return null;
	}

	public static List<IExtractorRecipe> getRecipeFromFirstInput(ItemStack input) {
		if(input.isEmpty())
			return Arrays.asList();
		List<IExtractorRecipe> list = new ArrayList<>();
		for (IExtractorRecipe recipes : getRecipes()) {
			if (recipes.isInput(input)) {
				list.add(recipes);
			}
		}
		return list;
	}

	public static List<IExtractorRecipe> getRecipeFromContainerInput(ItemStack input) {
		if(input.isEmpty())
			return Arrays.asList();
		List<IExtractorRecipe> list = new ArrayList<>();
		for (IExtractorRecipe recipes : getRecipes()) {
			if (recipes.getInputContainer().size() > 0 && recipes.isInputContainer(input)) {
				list.add(recipes);
			}
		}
		return list;
	}

}
