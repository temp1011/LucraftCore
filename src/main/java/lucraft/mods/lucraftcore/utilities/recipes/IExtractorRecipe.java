package lucraft.mods.lucraftcore.utilities.recipes;

import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidStack;

import java.util.List;

public interface IExtractorRecipe {

    ItemStack getPrimaryResult();
    float getPrimaryChance();

    ItemStack getSecondaryResult();
    float getSecondaryChance();

    boolean isInput(ItemStack stack);
    boolean isInputContainer(ItemStack stack);
    boolean isRequiredFluid(FluidStack fluid);
    List<ItemStack> getInput();
    List<ItemStack> getInputContainer();
    FluidStack getRequiredFluid();

    ResourceLocation getRegistryName();

}