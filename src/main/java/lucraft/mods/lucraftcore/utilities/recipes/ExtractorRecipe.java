package lucraft.mods.lucraftcore.utilities.recipes;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.oredict.OreDictionary;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExtractorRecipe implements IExtractorRecipe {

	public Object input;
	public Object inputContainer;
	public FluidStack fluid;
	public ItemStack primaryOutput;
	public float primaryChance;
	public ItemStack secondaryOutput;
	public float secondaryChance;
	private ResourceLocation registryName;

	public ExtractorRecipe(Object input, Object inputContainer, FluidStack requiredFluid, ItemStack primaryOutput, float primaryChance, ItemStack secondaryOutput, float secondaryChance) {
		this.input = input;
		this.inputContainer = inputContainer;
		this.fluid = requiredFluid;
		this.primaryOutput = primaryOutput;
		this.primaryChance = primaryChance;
		this.secondaryOutput = secondaryOutput;
		this.secondaryChance = secondaryChance;
	}

	@Override
	public ItemStack getPrimaryResult() {
		return this.primaryOutput == null ? ItemStack.EMPTY : this.primaryOutput;
	}

	@Override
	public float getPrimaryChance() {
		return this.primaryChance;
	}

	@Override
	public ItemStack getSecondaryResult() {
		return this.secondaryOutput == null ? ItemStack.EMPTY : this.secondaryOutput;
	}

	@Override
	public float getSecondaryChance() {
		return this.secondaryChance;
	}

	@Override
	public boolean isInput(ItemStack stack) {
		if (this.input instanceof Item)
			return stack.getItem() == (Item) this.input;
		else if (this.input instanceof Block)
			return stack.getItem() == Item.getItemFromBlock((Block) this.input);
		else if (this.input instanceof ItemStack)
			return OreDictionary.itemMatches(stack, (ItemStack) this.input, false);
		else if (this.input instanceof String) {
			for (ItemStack s : OreDictionary.getOres((String) this.input)) {
				if (OreDictionary.itemMatches(stack, s, false)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean isInputContainer(ItemStack stack) {
		if(this.inputContainer == null)
			return true;
		else if (this.inputContainer instanceof Item)
			return stack.getItem() == (Item) this.inputContainer;
		else if (this.inputContainer instanceof Block)
			return stack.getItem() == Item.getItemFromBlock((Block) this.inputContainer);
		else if (this.inputContainer instanceof ItemStack)
			return OreDictionary.itemMatches(stack, (ItemStack) this.inputContainer, false);
		else if (this.inputContainer instanceof String) {
			for (ItemStack s : OreDictionary.getOres((String) this.inputContainer)) {
				if (OreDictionary.itemMatches(stack, s, false)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean isRequiredFluid(FluidStack fluid) {
		if(this.fluid == null)
			return true;
		return this.fluid.isFluidEqual(fluid) && fluid.amount >= this.fluid.amount;
	}

	@Override
	public List<ItemStack> getInput() {
		if (this.input instanceof Item)
			return Arrays.asList(new ItemStack((Item) this.input));
		else if (this.input instanceof Block)
			return Arrays.asList(new ItemStack((Block) this.input));
		else if (this.input instanceof ItemStack)
			return Arrays.asList((ItemStack) this.input);
		else if (this.input instanceof String) {
			List<ItemStack> list = new ArrayList<>();
			for (ItemStack s : OreDictionary.getOres((String) this.input)) {
				list.add(s);
			}
			return list;
		}
		return Arrays.asList();
	}

	@Override
	public List<ItemStack> getInputContainer() {
		if(this.inputContainer == null)
			return Arrays.asList();
		else if (this.inputContainer instanceof Item)
			return Arrays.asList(new ItemStack((Item) this.inputContainer));
		else if (this.inputContainer instanceof Block)
			return Arrays.asList(new ItemStack((Block) this.inputContainer));
		else if (this.inputContainer instanceof ItemStack)
			return Arrays.asList((ItemStack) this.inputContainer);
		else if (this.inputContainer instanceof String) {
			List<ItemStack> list = new ArrayList<>();
			for (ItemStack s : OreDictionary.getOres((String) this.inputContainer)) {
				list.add(s);
			}
			return list;
		}
		return Arrays.asList();
	}

	@Override
	public FluidStack getRequiredFluid() {
		return this.fluid;
	}

	@Override
	public ResourceLocation getRegistryName() {
		return null;
	}

	public ExtractorRecipe setRegistryName(ResourceLocation name) {
		this.registryName = name;
		return this;
	}

}
