package lucraft.mods.lucraftcore.utilities.recipes;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.addonpacks.AddonPackReadEvent;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.apache.commons.io.FilenameUtils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Mod.EventBusSubscriber(modid = LucraftCore.MODID)
public class AddonPackExtractorRecipeReader {

	public static Map<ResourceLocation, JsonObject> recipes = new HashMap<>();

	@SubscribeEvent
	public static void onRead(AddonPackReadEvent e) {
		if (e.getDirectory().equals("extractorrecipes") && FilenameUtils.getExtension(e.getFileName()).equalsIgnoreCase("json")) {
			BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(e.getInputStream(), StandardCharsets.UTF_8));
			JsonObject jsonobject = (new JsonParser()).parse(bufferedreader).getAsJsonObject();
			recipes.put(e.getResourceLocation(), jsonobject);
		}
	}

	public static void loadRecipes() {
		for (ResourceLocation loc : recipes.keySet()) {
			JsonObject jsonobject = recipes.get(loc);

			Object input = deserializeItemStackWithOreDic(JsonUtils.getJsonObject(jsonobject, "input"));

			Object container = null;
			if(JsonUtils.hasField(jsonobject, "container"))
			    container = deserializeItemStackWithOreDic(JsonUtils.getJsonObject(jsonobject, "container"));

			FluidStack fluid = null;
            if(JsonUtils.hasField(jsonobject, "fluid"))
                fluid = deserializeFluidStack(JsonUtils.getJsonObject(jsonobject, "fluid"));

			JsonObject outputs = JsonUtils.getJsonObject(jsonobject, "outputs");
			ItemStack primaryOutput = deserializeItemStack(JsonUtils.getJsonObject(outputs, "primary"));
			float primaryChance = JsonUtils.getFloat(JsonUtils.getJsonObject(outputs, "primary"), "chance", 1F);
			ItemStack secondaryOutput = null;
			float secondaryChance = 0F;

			if (JsonUtils.hasField(outputs, "secondary")) {
				secondaryOutput = deserializeItemStack(JsonUtils.getJsonObject(outputs, "secondary"));
				secondaryChance = JsonUtils.getFloat(JsonUtils.getJsonObject(outputs, "secondary"), "chance", 1F);
			}

			ExtractorRecipe recipe = new ExtractorRecipe(input, container, fluid, primaryOutput, primaryChance, secondaryOutput, secondaryChance).setRegistryName(loc);
			ExtractorRecipeHandler.addRecipe(recipe);
		}

		recipes.clear();
	}

	private static ItemStack deserializeItemStack(JsonObject object) {
		if (!object.has("item")) {
			throw new JsonSyntaxException("Unsupported item, add 'item' key");
		} else {
			Item item = JsonUtils.getItem(object, "item");
			int i = JsonUtils.getInt(object, "data", 0);
			ItemStack ret = new ItemStack(item, JsonUtils.getInt(object, "amount", 1), i);
			ret.setTagCompound(net.minecraftforge.common.util.JsonUtils.readNBT(object, "nbt"));
			return ret;
		}
	}

	private static Object deserializeItemStackWithOreDic(JsonObject object) {
		if (!object.has("item") && !object.has("ore")) {
			throw new JsonSyntaxException("Unsupported item, add 'item' or 'ore' key");
		} else {
			if (object.has("item")) {
				Item item = JsonUtils.getItem(object, "item");
				int i = JsonUtils.getInt(object, "data", 0);
				ItemStack ret = new ItemStack(item, JsonUtils.getInt(object, "amount", 1), i);
				ret.setTagCompound(net.minecraftforge.common.util.JsonUtils.readNBT(object, "nbt"));
				return ret;
			} else if (object.has("ore")) {
				return JsonUtils.getString(object, "ore");
			}
		}

		return null;
	}

	private static FluidStack deserializeFluidStack(JsonObject object) {
		if (!object.has("fluid")) {
			throw new JsonSyntaxException("Unsupported fluid, add 'fluid' key");
		} else {
			Fluid fluid = FluidRegistry.getFluid(JsonUtils.getString(object, "fluid"));
			FluidStack stack = new FluidStack(fluid, JsonUtils.getInt(object, "amount", 1));
			return stack;
		}
	}

}
