package lucraft.mods.lucraftcore.utilities.jei.information;

import lucraft.mods.lucraftcore.utilities.jei.JEIInfoReader;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InformationWrapper implements IRecipeWrapper {

	public final JEIInfoReader.JEIInfo info;

	public InformationWrapper(JEIInfoReader.JEIInfo info) {
		this.info = info;
	}

	@Override
	public void getIngredients(IIngredients ingredients) {
		List<List<ItemStack>> list = new ArrayList<>();
		for (ItemStack s : this.info.getItemStacks())
			list.add(Arrays.asList(s));
		ingredients.setOutputLists(ItemStack.class, list);
	}

	@Override
	public void drawInfo(Minecraft minecraft, int recipeWidth, int recipeHeight, int mouseX, int mouseY) {
		{
			int i = this.info.getItemStacks().length;
			int length = i * 18;

			for (int j = 0; j < i; j++) {
				InformationCategory.slot.draw(minecraft, 69 - (length / 2) + (j * 18), 5);
			}
		}

		{
			List<String> text = minecraft.fontRenderer.listFormattedStringToWidth(this.info.getText().getFormattedText(), 138);
			for (int i = 0; i < text.size(); i++) {
				minecraft.fontRenderer.drawString(text.get(i), 0, 30 + i * 10, 4210752);
			}
		}
	}

	public static List<InformationWrapper> getRecipes() {
		List<InformationWrapper> infos = new ArrayList<>();

		for (JEIInfoReader.JEIInfo i : JEIInfoReader.getInfo().values()) {
			infos.add(new InformationWrapper(i));
		}

		return infos;
	}
}
