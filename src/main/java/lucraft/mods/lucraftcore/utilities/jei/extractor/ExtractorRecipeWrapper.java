package lucraft.mods.lucraftcore.utilities.jei.extractor;

import lucraft.mods.lucraftcore.utilities.recipes.ExtractorRecipeHandler;
import lucraft.mods.lucraftcore.utilities.recipes.IExtractorRecipe;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExtractorRecipeWrapper implements IRecipeWrapper {

	public final IExtractorRecipe recipe;

	public ExtractorRecipeWrapper(IExtractorRecipe recipe) {
		this.recipe = recipe;
	}

	@Override
	public void getIngredients(IIngredients ingredients) {
		ingredients.setInputLists(ItemStack.class, Arrays.asList(recipe.getInput(), recipe.getInputContainer()));
		ingredients.setInput(FluidStack.class, recipe.getRequiredFluid());
		ingredients.setOutputLists(ItemStack.class, Arrays.asList(Arrays.asList(recipe.getPrimaryResult()), Arrays.asList(recipe.getSecondaryResult())));
	}

	public static List<ExtractorRecipeWrapper> getRecipes() {
		List<ExtractorRecipeWrapper> recipes = new ArrayList<>();

		for (IExtractorRecipe recipe : ExtractorRecipeHandler.getRecipes()) {
			recipes.add(new ExtractorRecipeWrapper(recipe));
		}

		return recipes;
	}
}
