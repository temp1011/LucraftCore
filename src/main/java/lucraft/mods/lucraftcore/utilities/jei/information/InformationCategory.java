package lucraft.mods.lucraftcore.utilities.jei.information;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import lucraft.mods.lucraftcore.utilities.jei.LCJEIPlugin;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.gui.IDrawable;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeCategory;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;

import javax.annotation.Nullable;
import java.util.List;

public class InformationCategory implements IRecipeCategory {

    private static final int output = 0;
    private final IDrawable background;
    private final IDrawable icon;
    public static IDrawable slot;

    public InformationCategory(IGuiHelper guiHelper) {
        background = guiHelper.createBlankDrawable(138, 138);
        icon = guiHelper.drawableBuilder(LCJEIPlugin.TEXTURE, 18, 80, 16, 16).build();
        slot = guiHelper.createDrawable(LCJEIPlugin.TEXTURE, 0, 80, 18, 18);
    }

    @Override
    public String getUid() {
        return LCJEIPlugin.INFORMATION;
    }

    @Override
    public String getTitle() {
        return StringHelper.translateToLocal("lucraftcore.jei.information");
    }

    @Override
    public String getModName() {
        return LucraftCore.NAME;
    }

    @Override
    public IDrawable getBackground() {
        return this.background;
    }

    @Nullable
    @Override
    public IDrawable getIcon() {
        return icon;
    }

    @Override
    public void drawExtras(Minecraft minecraft) {

    }

    @Override
    public void setRecipe(IRecipeLayout recipeLayout, IRecipeWrapper recipeWrapper, IIngredients ingredients) {
        IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();

        List<List<ItemStack>> list = ingredients.getOutputs(ItemStack.class);
        int length = list.size() * 18;

        int i = 0;
        for(List<ItemStack> l : list) {
            guiItemStacks.init(i, false, 69 - (length / 2) + (i * 18), 5);
            i++;
        }

        guiItemStacks.set(ingredients);
    }
}
