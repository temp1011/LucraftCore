package lucraft.mods.lucraftcore.superpowers.abilities;

import com.google.gson.JsonObject;
import lucraft.mods.lucraftcore.sizechanging.capabilities.CapabilitySizeChanging;
import lucraft.mods.lucraftcore.sizechanging.capabilities.ISizeChanging;
import lucraft.mods.lucraftcore.sizechanging.sizechanger.SizeChanger;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;

public class AbilitySizeChange extends AbilityToggle {

	private float size;
	private SizeChanger sizeChanger;

	public AbilitySizeChange(EntityPlayer player) {
		super(player);
	}

	public AbilitySizeChange(EntityPlayer player, float size) {
		this(player, size, null);
	}

	public AbilitySizeChange(EntityPlayer player, float size, SizeChanger sizeChanger) {
		super(player);
		this.size = size;
		this.sizeChanger = sizeChanger;
	}

	@Override
	public void readFromAddonPack(JsonObject data, List<Ability> abilities) {
		super.readFromAddonPack(data, abilities);
		this.size = JsonUtils.getFloat(data, "size");
		this.sizeChanger = JsonUtils.hasField(data, "size_changer") ? SizeChanger.SIZE_CHANGER_REGISTRY.getValue(new ResourceLocation(JsonUtils.getString(data, "size_changer"))) : null;
	}

	@Override
	public String getDisplayDescription() {
		return super.getDisplayDescription() + "\n \n" + TextFormatting.BLUE + getSize();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		LCRenderHelper.drawIcon(mc, gui, x, y, 0, 10);
	}

	public float getSize() {
		return size;
	}

	public SizeChanger getSizeChanger() {
		return sizeChanger;
	}

	@Override
	public boolean action() {
		if(!player.hasCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null))
			return false;

		ISizeChanging data = player.getCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null);
		float f = isEnabled() ? 1F : this.size;

		if(this.sizeChanger == null)
			data.setSize(f);
		else
			data.setSize(f, this.sizeChanger);

		return true;
	}

	@Override
	public void updateTick() {

	}

	@Override
	public boolean isEnabled() {
		if(!player.hasCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null))
			return false;
		return player.getCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null).getSize() != 1F;
	}

}