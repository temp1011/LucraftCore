package lucraft.mods.lucraftcore.superpowers.abilities;

import com.google.gson.JsonObject;

import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;

public class AbilityHealing extends AbilityConstant {

	private int frequency;
	private float amount;
	private boolean fixed = false;

	public AbilityHealing(EntityPlayer player) {
		this(player, 20, 0.5f);
	}

	public AbilityHealing(EntityPlayer player, int frequency, float amount) {
		super(player);
		this.frequency = frequency;
		this.amount = amount;
	}

	public AbilityHealing(EntityPlayer player, int frequency, float amount, boolean fixed) {
		super(player);
		this.frequency = frequency;
		this.amount = amount;
		this.fixed = true;
	}

	@Override
	public void readFromAddonPack(JsonObject data, List<Ability> abilities) {
		super.readFromAddonPack(data, abilities);
		this.frequency = JsonUtils.getInt(data, "frequency");
		this.amount = JsonUtils.getFloat(data, "amount");
	}

	@Override
	public String getDisplayDescription() {
		return super.getDisplayDescription() + "\n \n" + TextFormatting.BLUE + StringHelper.translateToLocal("lucraftcore.info.interval") + ": " + (getFrequency() / 20F) + "\n" + TextFormatting.BLUE + StringHelper.translateToLocal("lucraftcore.info.amount") + ": " + getAmount() + " " + StringHelper.translateToLocal("lucraftcore.info.hearts");
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		LCRenderHelper.drawIcon(mc, gui, x, y, 0, 0);
	}

	@Override
	public boolean showInAbilityBar() {
		return false;
	}

	@Override
	public void updateTick() {
		if (frequency != 0 && ticks % frequency == 0) {
			player.heal(amount);
		}
	}

	public int getFrequency() {
		return frequency;
	}

	public float getAmount() {
		return amount;
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt) {
		super.deserializeNBT(nbt);
		if (!fixed) {
			this.frequency = nbt.getInteger("Frequency");
			this.amount = nbt.getFloat("Amount");
		}
	}

	@Override
	public NBTTagCompound serializeNBT() {
		NBTTagCompound nbt = super.serializeNBT();
		nbt.setInteger("Frequency", frequency);
		nbt.setFloat("Amount", amount);
		return nbt;
	}

}
