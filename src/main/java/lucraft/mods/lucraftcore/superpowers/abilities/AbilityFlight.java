package lucraft.mods.lucraftcore.superpowers.abilities;

import com.google.gson.JsonObject;
import lucraft.mods.lucraftcore.LCConfig;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.sizechanging.capabilities.CapabilitySizeChanging;
import lucraft.mods.lucraftcore.superpowers.ModuleSuperpowers;
import lucraft.mods.lucraftcore.util.events.RenderModelEvent;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.model.ModelPlayer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;

public class AbilityFlight extends AbilityToggle
{

	public double speed;
	public double sprintSpeed;
	public boolean rotateArms;

	public AbilityFlight(EntityPlayer player)
	{
		this(player, 1D, 5D, true);
	}

	public AbilityFlight(EntityPlayer player, double speed, double sprintSpeed, boolean rotateArms)
	{
		super(player);
		this.speed = speed;
		this.sprintSpeed = sprintSpeed;
		this.rotateArms = rotateArms;
	}

	@Override
	public void readFromAddonPack(JsonObject data, List<Ability> abilities)
	{
		super.readFromAddonPack(data, abilities);

		this.speed = JsonUtils.getFloat(data, "speed", 1);
		this.sprintSpeed = JsonUtils.getFloat(data, "speed", (float) (this.speed * 3));
		this.rotateArms = JsonUtils.getBoolean(data, "rotate_arms", true);
	}

	@Override
	public void firstTick()
	{
		super.firstTick();
	}

	@Override
	public void updateTick()
	{
		if (player.onGround && ticks > 20)
			this.setEnabled(false);

		if (player.moveForward > 0F && !player.onGround)
		{
			Vec3d vec = player.getLookVec();
			double speed = player.isSprinting() ? this.sprintSpeed : this.speed;
			if (LCConfig.modules.size_changing)
				speed *= player.getCapability(CapabilitySizeChanging.SIZE_CHANGING_CAP, null).getSize();
			player.motionX = vec.x * speed;
			player.motionY = vec.y * speed;
			player.motionZ = vec.z * speed;

			if (!player.world.isRemote)
			{
				player.fallDistance = 0.0F;
			}
		}
		else
		{
			float motionY = 0F;
			if (ticks < 20)
			{
				int lowestY = player.getPosition().getY();

				while (lowestY > 0 && !player.world.isBlockFullCube(new BlockPos(player.posX, lowestY, player.posZ)))
				{
					lowestY--;
				}

				if (player.getPosition().getY() - lowestY < 5)
				{
					motionY += 1;
				}
			}

			motionY += Math.sin(player.ticksExisted / 10F) / 100F;
			player.fallDistance = 0F;
			player.motionY = motionY;
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void drawIcon(Minecraft mc, Gui gui, int x, int y)
	{
		LCRenderHelper.drawIcon(mc, gui, x, y, 1, 0);
	}

	@EventBusSubscriber(modid = LucraftCore.MODID, value = Side.CLIENT)
	public static class Renderer
	{

		@SubscribeEvent
		public static void onRenderModel(RenderModelEvent e)
		{
			if (!ModuleSuperpowers.INSTANCE.isEnabled())
				return;

			if (e.getEntityLiving() instanceof EntityPlayer)
			{
				for (AbilityFlight ab : Ability
						.getAbilitiesFromClass(Ability.getCurrentPlayerAbilities((EntityPlayer) e.getEntityLiving()), AbilityFlight.class))
				{
					if (ab != null && ab.isUnlocked() && ab.isEnabled())
					{
						EntityPlayer player = (EntityPlayer) e.getEntityLiving();
						float speed = (float) MathHelper.clamp(Math
								.sqrt((player.prevPosX - player.posX) * (player.prevPosX - player.posX) + (player.prevPosZ - player.posZ) * (player.prevPosZ
										- player.posZ) + (player.prevPosY - player.posY) * (player.prevPosY - player.posY)), 0F, 1F);
						GlStateManager.rotate(speed * (90F + player.rotationPitch), 1, 0, 0);
						break;
					}
				}
			}
		}

		@SubscribeEvent(receiveCanceled = true)
		public static void onSetUpModel(RenderModelEvent.SetRotationAngels e)
		{
			if (!ModuleSuperpowers.INSTANCE.isEnabled())
				return;

			if (e.getEntity() instanceof EntityPlayer)
			{
				EntityPlayer player = (EntityPlayer) e.getEntity();
				for (AbilityFlight ab : Ability.getAbilitiesFromClass(Ability.getCurrentPlayerAbilities((EntityPlayer) e.getEntity()), AbilityFlight.class))
				{
					if (ab != null && ab.isUnlocked() && ab.isEnabled() && player.moveForward > 0F)
					{
						float speed = (float) MathHelper.clamp(Math
								.sqrt((player.prevPosX - player.posX) * (player.prevPosX - player.posX) + (player.prevPosZ - player.posZ) * (player.prevPosZ
										- player.posZ) + (player.prevPosY - player.posY) * (player.prevPosY - player.posY)), 0F, 1F);
						e.setCanceled(true);
						float rotation = speed * (90F + player.rotationPitch);
						e.model.bipedHead.rotateAngleX -= Math.toRadians(rotation);
						e.model.bipedHeadwear.rotateAngleX = e.model.bipedHead.rotateAngleX;

						e.model.bipedRightArm.rotateAngleX = (float) Math.toRadians(ab.rotateArms ? 180F : 0F);
						e.model.bipedLeftArm.rotateAngleX = e.model.bipedRightArm.rotateAngleX;
						e.model.bipedRightLeg.rotateAngleX = 0;
						e.model.bipedLeftLeg.rotateAngleX = 0;

						if (e.model instanceof ModelPlayer)
						{
							ModelPlayer model = (ModelPlayer) e.model;
							model.bipedRightArmwear.rotateAngleX = model.bipedRightArm.rotateAngleX;
							model.bipedLeftArmwear.rotateAngleX = model.bipedLeftArm.rotateAngleX;
							model.bipedRightLegwear.rotateAngleX = model.bipedRightLeg.rotateAngleX;
							model.bipedLeftLegwear.rotateAngleX = model.bipedLeftLeg.rotateAngleX;
						}
					}
				}
			}
		}

	}

}
