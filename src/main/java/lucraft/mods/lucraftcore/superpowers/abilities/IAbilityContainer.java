package lucraft.mods.lucraftcore.superpowers.abilities;

import java.util.List;

public interface IAbilityContainer {

	public List<Ability> getAbilities();

}
