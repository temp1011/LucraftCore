package lucraft.mods.lucraftcore.superpowers.abilitybar;

import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import lucraft.mods.lucraftcore.superpowers.SuperpowerPlayerHandler;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.capabilities.CapabilitySuperpower;
import lucraft.mods.lucraftcore.util.abilitybar.IAbilityBarEntry;
import lucraft.mods.lucraftcore.util.abilitybar.IAbilityBarProvider;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;

import java.util.ArrayList;
import java.util.List;

public class SuperpowerAbilityBarProvider implements IAbilityBarProvider {

	@Override
	public List<IAbilityBarEntry> getEntries() {
		EntityPlayer player = Minecraft.getMinecraft().player;
		SuperpowerPlayerHandler handler = SuperpowerHandler.getSuperpowerPlayerHandler(player);
		CapabilitySuperpower.SuitSetAbilityHandler suitHandler = SuperpowerHandler.getSuitSetAbilityHandler(player);

		List<IAbilityBarEntry> list = new ArrayList<>();

		if (handler != null) {
			for (int i = 0; i < handler.getAbilities().size(); i++) {
				Ability ab = handler.getAbilities().get(i);

				if (ab.showInAbilityBar() && !ab.isHidden() && ab.isUnlocked() && !ab.alwaysHidden())
					list.add(new SuperpowerAbilityBarEntry(ab, i));
			}
		}

		if (suitHandler != null) {
			for (int i = 0; i < suitHandler.getAbilities().size(); i++) {
                Ability ab = suitHandler.getAbilities().get(i);

                if (ab.showInAbilityBar() && !ab.isHidden() && ab.isUnlocked() && !ab.alwaysHidden())
                    list.add(new SuperpowerAbilityBarEntry(ab, i));
			}
		}

		return list;
	}

}
