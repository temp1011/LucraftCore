package lucraft.mods.lucraftcore.superpowers.abilitybar;

import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.events.AbilityKeyEvent;
import lucraft.mods.lucraftcore.superpowers.network.MessageAbilityKey;
import lucraft.mods.lucraftcore.util.abilitybar.IAbilityBarEntry;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraftforge.common.MinecraftForge;

public class SuperpowerAbilityBarEntry implements IAbilityBarEntry {

	public Ability ability;
	public int index;

	public SuperpowerAbilityBarEntry(Ability ability, int index) {
		this.ability = ability;
		this.index = index;
	}

	@Override
	public boolean isActive() {
		return true;
	}

	@Override
	public void onButtonPress() {
		if (MinecraftForge.EVENT_BUS.post(new AbilityKeyEvent.Client(ability, true)))
			return;
		LCPacketDispatcher.sendToServer(new MessageAbilityKey(true, this.index, this.ability.context));
	}

	@Override
	public void onButtonRelease() {
		if (MinecraftForge.EVENT_BUS.post(new AbilityKeyEvent.Client(ability, false)))
			return;
		LCPacketDispatcher.sendToServer(new MessageAbilityKey(false, this.index, this.ability.context));
	}

	@Override
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		this.ability.drawIcon(mc, gui, x, y);
		this.ability.drawAdditionalInfo(mc, gui, x, y);
	}

	@Override
	public String getDescription() {
		return this.ability.getDisplayName();
	}

	@Override
	public boolean renderCooldown() {
		return this.ability.renderCooldown();
	}

	@Override
	public float getCooldownPercentage() {
		return this.ability.getCooldownPercentage();
	}

	@Override
	public boolean showKey() {
		return this.ability.showKeyInAbilityBar();
	}
}
