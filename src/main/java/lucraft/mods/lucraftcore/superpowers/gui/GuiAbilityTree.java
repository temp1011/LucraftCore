package lucraft.mods.lucraftcore.superpowers.gui;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.superpowers.ISuperpowerAbilityCoordinates;
import lucraft.mods.lucraftcore.superpowers.Superpower;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import lucraft.mods.lucraftcore.superpowers.SuperpowerPlayerHandler;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.gui.InventoryTabSuperpowerAbilities;
import lucraft.mods.lucraftcore.util.gui.buttons.GuiButton10x;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import micdoodle8.mods.galacticraft.api.client.tabs.TabRegistry;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec2f;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.client.config.GuiButtonExt;
import org.lwjgl.opengl.GL11;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GuiAbilityTree extends GuiScreen {

	public static final ResourceLocation TEX = new ResourceLocation(LucraftCore.MODID, "textures/gui/abilities.png");

	public EntityPlayer player;
	public Superpower superpower;
	public SuperpowerPlayerHandler data;

	public int offsetX;
	public int offsetY;

	public int prevMouseX;
	public int prevMouseY;
	
	public int selectedAbility = -1;
	
	private int xSize = 256;
	private int ySize = 189;

	public GuiAbilityTree(EntityPlayer player) {
		this.player = player;
		this.superpower = SuperpowerHandler.getSuperpower(player);
		this.data = SuperpowerHandler.getSuperpowerPlayerHandler(player);
	}

	@Override
	public void initGui() {
		super.initGui();

		int i = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;

		GuiButton customizer = new GuiButtonExt(1, i + 20, j + 165, 70, 18, StringHelper.translateToLocal("lucraftcore.info.customizer"));
		customizer.enabled = superpower.canCustomize();
		this.buttonList.add(customizer);
		this.buttonList.add(new GuiButton10x(2, i + 239, j + 90, "?"));

		TabRegistry.updateTabValues(i, j, InventoryTabSuperpowerAbilities.class);
		TabRegistry.addTabsToList(this.buttonList);

	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		super.actionPerformed(button);

		if (button.id == 1)
			mc.displayGuiScreen(superpower.getCustomizerGui(player));

	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		this.drawDefaultBackground();
		
		int i = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;
		int x = i + xSize / 2 + offsetX;
		int y = j + ySize / 2 + offsetY;

		GlStateManager.color(1, 1, 1);

		// Apply Background Color
		if (superpower.getPlayerRenderer() != null)
			superpower.getPlayerRenderer().applyColor();
		
		// Draw Background
		mc.getTextureManager().bindTexture(TEX);
		this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize);
		GlStateManager.color(1, 1, 1);

		// Draw Superpower Title
		String name = TextFormatting.UNDERLINE + superpower.getDisplayName();
		int titleX = this.xSize / 2 - mc.fontRenderer.getStringWidth(name) / 2;
		mc.fontRenderer.drawString(name, i + titleX, j + 10, 0x373737);

		GlStateManager.pushMatrix();
		GL11.glEnable(GL11.GL_SCISSOR_TEST);
		ScaledResolution res = new ScaledResolution(mc);
		GL11.glScissor(mc.displayWidth / 2 - (xSize - 42) / 2 * res.getScaleFactor(), mc.displayHeight / 2 - (ySize - 56) / 2 * res.getScaleFactor() - 1, 214 * res.getScaleFactor(), 133 * res.getScaleFactor());
		
		// Draw stone background
		TextureAtlasSprite sprite = Minecraft.getMinecraft().getBlockRendererDispatcher().getBlockModelShapes().getTexture(getBackgroundBlockState());
		GlStateManager.color(0.6F, 0.6F, 0.6F);
		this.mc.getTextureManager().bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
		for (int blocks1 = 0; blocks1 < 33; blocks1++) {
			for (int blocks2 = 0; blocks2 < 28; blocks2++) {
				this.drawTexturedModalRect(x - 257 + blocks1 * 16, y - 216 + blocks2 * 16, sprite, 16, 16);
			}
		}
		GlStateManager.color(1, 1, 1);
		
		if (superpower instanceof ISuperpowerAbilityCoordinates) {
			ISuperpowerAbilityCoordinates coordinates = (ISuperpowerAbilityCoordinates) superpower;
			
			for (Ability ab : data.getAbilities()) {
				Vec2f vec = coordinates.getDisplayCoordinatesForAbility(ab);
				
				if (vec != null) {
					int abX = (int) (x + vec.x * 29);
					int abY = (int) (y + vec.y * 29);
					
					mc.getTextureManager().bindTexture(TEX);
					
					// Selected = Draw black box behind
					if (isSelected(ab)) {
						GlStateManager.pushMatrix();
						GlStateManager.enableBlend();
						this.drawTexturedModalRect(abX - 3, abY - 3, 83, 189, 28, 28);
						GlStateManager.disableBlend();
						GlStateManager.popMatrix();
					}
					
					if (ab.getDependentAbility() != null && coordinates.getDisplayCoordinatesForAbility(ab.getDependentAbility()) != null) {
						Vec2f vecStart = coordinates.getDisplayCoordinatesForAbility(ab.getDependentAbility());
						int startX = (int) (x + vecStart.x * 29 + 10);
						int endX = abX + 10;
						int startY = (int) (y + vecStart.y * 29 + 22);
						int endY = abY;

						mc.getTextureManager().bindTexture(TEX);
						
						/** Draw horizontal line **/
						// If child ab is below parent
						if (vec.y > vecStart.y) {
							if (vec.x > vecStart.x) // +ve
								for (int xLine = Math.min(startX + 12, endX); xLine < Math.max(startX, endX); xLine++)
									this.drawTexturedModalRect(xLine, startY - 12, 48, 204, 1, 1);
							else // -ve
								for (int xLine = Math.min(startX, endX); xLine < Math.max(startX - 10, endX); xLine++)
									this.drawTexturedModalRect(xLine, startY - 12, 48, 204, 1, 1);
						}
						// If child ab is at level with parent
						else if (vec.y == vecStart.y) {
							if (vec.x > vecStart.x) // +ve
								for (int xLine = Math.min(startX + 12, endX); xLine < Math.max(startX, endX - 10); xLine++)
									this.drawTexturedModalRect(xLine, endY + 10, 48, 204, 1, 1);
							else // -ve
								for (int xLine = Math.min(startX, endX + 12); xLine < Math.max(startX - 10, endX); xLine++)
									this.drawTexturedModalRect(xLine, endY + 10, 48, 204, 1, 1);
						}
						// If child ab is above parent
						else if (vec.y < vecStart.y) {
							if (vec.x > vecStart.x) // +ve
								for (int xLine = Math.min(startX, endX); xLine < Math.max(startX, endX + 1); xLine++)
									this.drawTexturedModalRect(xLine, startY - 12, 48, 204, 1, 1);
							else // -ve
								for (int xLine = Math.min(startX, endX); xLine < Math.max(startX - 10, endX); xLine++)
									this.drawTexturedModalRect(xLine, startY - 12, 48, 204, 1, 1);
						}

						/** Draw vertical line **/
						// If child ab is below parent
						if (vec.y > vecStart.y) {
							for (int yLine = Math.min(startY - 12, endY); yLine < Math.max(startY, endY); yLine++) {
								this.drawTexturedModalRect(endX, yLine, 48, 204, 1, 1);
							}
						}
						// If child ab is above parent
						else if (vec.y < vecStart.y) {
							for (int yLine = Math.min(startY, endY + 22); yLine < Math.max(startY - 12, endY); yLine++) {
								this.drawTexturedModalRect(endX, yLine, 48, 204, 1, 1);
							}
						}
					}
				}
			}
			
			for (Ability ab : data.getAbilities()) {
				Vec2f vec = coordinates.getDisplayCoordinatesForAbility(ab);
				
				if (vec != null) {
					int abX = (int) (x + vec.x * 29);
					int abY = (int) (y + vec.y * 29);

					if (ab.getDependentAbility() != null && coordinates.getDisplayCoordinatesForAbility(ab.getDependentAbility()) != null) {
						Vec2f vecStart = coordinates.getDisplayCoordinatesForAbility(ab.getDependentAbility());
						int startX = (int) (x + vecStart.x * 29 + 10);
						int endX = (abX + 10);
						int startY = (int) (y + vecStart.y * 29 + 22);
						int endY = (abY);

						mc.getTextureManager().bindTexture(TEX);

						/** Draw Arrows **/
						// If child ab is below parent ab
						if (vec.y > vecStart.y) {
							this.drawTexturedModalRect(endX - 5, endY - 7, 58, 189, 11, 7);
						}
						// If child ab is above
						else if (vec.y < vecStart.y) {
							this.drawTexturedModalRect(endX - 5, endY + 22, 58, 196, 11, 7);
						}
						// If child ab is on same level as parent ab
						else if (vec.y == vecStart.y) {
							// If child ab is below parent ab
							if (vec.x > vecStart.x) { // If child ab is on the right of parent ab (+ve of X)
								this.drawTexturedModalRect(endX - 17, endY + 5, 76, 189, 7, 11);
							}
							// If child ab is on the left of parent ab (-ve of X)
							else {
								this.drawTexturedModalRect(endX + 12, endY + 5, 69, 189, 7, 11);
							}
						}
					}

					mc.getTextureManager().bindTexture(TEX);
					this.drawTexturedModalRect(abX, abY, 0, 189, 22, 22); // Draw ab button
					ab.drawIcon(mc, this, abX + 3, abY + 3); // Draw ab icon
					if (!ab.isUnlocked()) {
						mc.getTextureManager().bindTexture(TEX);
						GlStateManager.color(1, 1, 1, 1);
						this.drawTexturedModalRect(abX + 6, abY + 4, 48, 189, 10, 14);
					}
				}
			}
		}
		GL11.glDisable(GL11.GL_SCISSOR_TEST);
		GlStateManager.popMatrix();

		GlStateManager.color(1, 1, 1);

		// Draw XP
		mc.getTextureManager().bindTexture(TEX);
		if (superpower.canLevelUp() && data != null) {
			this.drawTexturedModalRect(i + 125, j + 171, 0, 215, 81, 5);
			float xp = (float) data.getXP() / (float) superpower.getXPForLevel(data.getLevel() + 1);
			if (data.getLevel() == superpower.getMaxLevel())
				xp = 1;
			this.drawTexturedModalRect(i + 125, j + 171, 0, 220, (int) (xp * 81), 5);

			LCRenderHelper.drawStringWithOutline("" + data.getLevel(), i + 112, j + 170, 0x98d06b, 0x081c11);

			if (data.getLevel() < superpower.getMaxLevel()) {
				boolean unicode = mc.fontRenderer.getUnicodeFlag();
				mc.fontRenderer.setUnicodeFlag(true);
				String xpProgress = data.getXP() + "/" + superpower.getXPForLevel(data.getLevel() + 1);
				int length = 120 - mc.fontRenderer.getStringWidth(xpProgress) / 2;
				mc.fontRenderer.drawString(xpProgress, i + length + 45, j + 175, 0x555555);
				mc.fontRenderer.setUnicodeFlag(unicode);
			}

		}
		
		super.drawScreen(mouseX, mouseY, partialTicks);

		// Draw ability name
		if (isInArea(mouseX, mouseY)) {
			if (superpower instanceof ISuperpowerAbilityCoordinates) {
				ISuperpowerAbilityCoordinates coordinates = (ISuperpowerAbilityCoordinates) superpower;

				for (Ability ab : data.getAbilities()) {
					Vec2f vec = coordinates.getDisplayCoordinatesForAbility(ab);

					if (vec != null) {
						int abX = (int) (x + vec.x * 30);
						int abY = (int) (y + vec.y * 30);

						if (mouseX >= abX && mouseX <= abX + 22 && mouseY >= abY && mouseY <= abY + 22) {
							LCRenderHelper.drawStringList(Arrays.asList(ab.getDisplayName()), mouseX + 10, mouseY, true);
						}
					}
				}
			}
		}
		
		// Draw desc list
		GuiButton info = this.buttonList.get(1);
		if (selectedAbility >= 0 && info.enabled && mouseX >= info.x && mouseX <= info.x + info.width && mouseY >= info.y && mouseY <= info.y + info.height) {
			Ability ability = data.getAbilities().get(selectedAbility);

			if (ability != null) {
				List<String> list = new ArrayList<String>();
				for (String s : ability.getDisplayDescription().split("\n")) {
					for (String s2 : mc.fontRenderer.listFormattedStringToWidth(s, 150)) {
						list.add(s2);
					}
				}
				if (mc.gameSettings.advancedItemTooltips)
					list.add(TextFormatting.DARK_GRAY + ability.getAbilityEntry().getRegistryName().toString());
				this.drawHoveringText(list, mouseX + 10, mouseY);
			}
		}
	}

	public boolean isSelected(Ability ability) {
		return data.getAbilities().contains(ability) && data.getAbilities().indexOf(ability) == selectedAbility;
	}

	@Override
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
		super.mouseClicked(mouseX, mouseY, mouseButton);

		int i = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;
		int x = i + xSize / 2 + offsetX;
		int y = j + ySize / 2 + offsetY;

		if (isInArea(mouseX, mouseY)) {
			if (superpower instanceof ISuperpowerAbilityCoordinates) {
				ISuperpowerAbilityCoordinates coordinates = (ISuperpowerAbilityCoordinates) superpower;

				for (Ability ab : data.getAbilities()) {
					Vec2f vec = coordinates.getDisplayCoordinatesForAbility(ab);

					if (vec != null) {
						int abX = (int) (x + vec.x * 30);
						int abY = (int) (y + vec.y * 30);

						if (mouseX >= abX && mouseX <= abX + 22 && mouseY >= abY && mouseY <= abY + 22) {
							selectedAbility = data.getAbilities().indexOf(ab);
							onAbilityClicked(ab);
						}
					}
				}
			}
		} else
			selectedAbility = -1;

		this.prevMouseX = mouseX;
		this.prevMouseY = mouseY;
	}

	@Override
	protected void mouseClickMove(int mouseX, int mouseY, int clickedMouseButton, long timeSinceLastClick) {
		if (isInArea(mouseX, mouseY)) {
			if (clickedMouseButton == 0) {
				offsetX = MathHelper.clamp(offsetX + mouseX - prevMouseX, -150, 150);
				offsetY = MathHelper.clamp(offsetY + mouseY - prevMouseY, -150, 150);

				this.prevMouseX = mouseX;
				this.prevMouseY = mouseY;
			}
		}
		super.mouseClickMove(mouseX, mouseY, clickedMouseButton, timeSinceLastClick);
	}
	
	public void onAbilityClicked(Ability ability) {
		
	}

	public IBlockState getBackgroundBlockState() {
		return Blocks.STONE.getDefaultState();
	}
	
	public boolean isInArea(int x, int y) {
		int i = (width - xSize) / 2;
		int j = (height - ySize) / 2;

		return x >= i + 21 && x < i + xSize - 21 && y >= j + 28 && y < j + ySize - 28;
	}
}