package lucraft.mods.lucraftcore.superpowers.events;

import lucraft.mods.lucraftcore.superpowers.Superpower;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.common.eventhandler.Cancelable;

public class PlayerSuperpowerEvent extends PlayerEvent {

	private final Superpower superpower;

	public PlayerSuperpowerEvent(EntityPlayer player, Superpower superpower) {
		super(player);
		this.superpower = superpower;
	}

	public Superpower getSuperpower() {
		return this.superpower;
	}

	public Superpower getPreviousSuperpower() {
		return SuperpowerHandler.getSuperpower(getEntityPlayer());
	}

	@Cancelable
	public static class PlayerGetsSuperpowerEvent extends PlayerSuperpowerEvent {

		public PlayerGetsSuperpowerEvent(EntityPlayer player, Superpower superpower) {
			super(player, superpower);
		}

	}

}
