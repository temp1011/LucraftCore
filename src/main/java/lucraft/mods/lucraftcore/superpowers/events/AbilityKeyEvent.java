package lucraft.mods.lucraftcore.superpowers.events;

import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.relauncher.Side;

public class AbilityKeyEvent extends Event {

	public Side side;
	public Ability ability;
	public boolean pressed;

	private AbilityKeyEvent(Ability ability, Side side, boolean pressed) {
		this.ability = ability;
		this.side = side;
		this.pressed = pressed;
	}

	@Cancelable
	public static class Client extends AbilityKeyEvent {

		public Client(Ability ability, boolean pressed) {
			super(ability, Side.CLIENT, pressed);
		}

	}

	@Cancelable
	public static class Server extends AbilityKeyEvent {

		public EntityPlayer player;

		public Server(Ability ability, EntityPlayer player, boolean pressed) {
			super(ability, Side.SERVER, pressed);
			this.player = player;
		}

	}

}
