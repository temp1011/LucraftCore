package lucraft.mods.lucraftcore.superpowers;

import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.events.AbilityKeyEvent;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.server.permission.DefaultPermissionLevel;
import net.minecraftforge.server.permission.PermissionAPI;

import java.util.HashMap;
import java.util.Map;

public class PermissionAbilityHandler {

    public static Map<ResourceLocation, String> PERMS = new HashMap<>();

    public static void init() {
        for(Ability.AbilityEntry entry : Ability.ABILITY_REGISTRY.getValuesCollection()) {
            String perm = "ability." + entry.getRegistryName().getResourceDomain() + "." + entry.getRegistryName().getResourcePath();
            PermissionAPI.registerNode(perm, DefaultPermissionLevel.ALL, "Ability '" + entry.getRegistryName().toString() + "'");
            PERMS.put(entry.getRegistryName(), perm);
        }
    }

    @SubscribeEvent
    public void onAbility(AbilityKeyEvent.Server e) {
        String perm = PERMS.get(e.ability.getAbilityEntry().getRegistryName());

        if(!PermissionAPI.hasPermission(e.player, perm)) {
            e.setCanceled(true);
            e.player.sendStatusMessage(new TextComponentTranslation("lucraftcore.info.no_perm_ability"), true);
        }
    }

}
