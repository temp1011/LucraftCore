package lucraft.mods.lucraftcore.superpowers;

import lucraft.mods.lucraftcore.superpowers.capabilities.ISuperpowerCapability;

public class JsonSuperpowerPlayerHandler extends SuperpowerPlayerHandler {

	public JsonSuperpowerPlayerHandler(ISuperpowerCapability cap, Superpower superpower) {
		super(cap, superpower);
	}

}
