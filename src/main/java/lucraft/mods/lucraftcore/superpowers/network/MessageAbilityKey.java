package lucraft.mods.lucraftcore.superpowers.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.CapabilityAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.IAdvancedCombatCapability;
import lucraft.mods.lucraftcore.network.AbstractServerMessageHandler;
import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import lucraft.mods.lucraftcore.superpowers.SuperpowerPlayerHandler;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.capabilities.CapabilitySuperpower;
import lucraft.mods.lucraftcore.superpowers.events.AbilityKeyEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageAbilityKey implements IMessage {

	public boolean pressed;
	public int index;
	public Ability.EnumAbilityContext context;

	public MessageAbilityKey() {
	}

	public MessageAbilityKey(boolean pressed, int index, Ability.EnumAbilityContext context) {
		this.pressed = pressed;
		this.index = index;
		this.context = context;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.pressed = buf.readBoolean();
		this.index = buf.readInt();
		this.context = Ability.EnumAbilityContext.values()[buf.readInt()];
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeBoolean(this.pressed);
		buf.writeInt(this.index);
		buf.writeInt(this.context.ordinal());
	}

	public static class Handler extends AbstractServerMessageHandler<MessageAbilityKey> {

		@Override
		public IMessage handleServerMessage(EntityPlayer player, MessageAbilityKey message, MessageContext ctx) {

			LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {
				if (message != null && ctx != null) {
					SuperpowerPlayerHandler handler = SuperpowerHandler.getSuperpowerPlayerHandler(player);
					CapabilitySuperpower.SuitSetAbilityHandler suitHandler = SuperpowerHandler.getSuitSetAbilityHandler(player);
					CapabilitySuperpower.AdvancedCombatAbilityHandler combatAbilityHandler = SuperpowerHandler.getAdvancedCombatAbilityHandler(player);
					if ((message.context == Ability.EnumAbilityContext.SUIT && suitHandler == null) || (message.context == Ability.EnumAbilityContext.SUPERPOWER && handler == null))
						return;


					if(message.index >= 0)
					{
						Ability ability = message.context == Ability.EnumAbilityContext.SUIT ?
								suitHandler.getAbilities().get(message.index) : message.context == Ability.EnumAbilityContext.SUPERPOWER ?
								handler.getAbilities().get(message.index) : combatAbilityHandler.getAbilities().get(message.index);

						if (ability == null || MinecraftForge.EVENT_BUS.post(new AbilityKeyEvent.Server(ability, player, message.pressed)))
							return;

						if (message.pressed)
						{
							ability.onKeyPressed();
						}
						else
						{
							ability.onKeyReleased();
						}
					} else {
						IAdvancedCombatCapability capability = player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
						if(capability == null)
							return;
						int index = message.index + 4;
						Ability ability = capability.getCombatBarAbilities()[index];

						if (ability == null || MinecraftForge.EVENT_BUS.post(new AbilityKeyEvent.Server(ability, player, message.pressed)))
							return;

						if (message.pressed)
						{
							ability.onKeyPressed();
						}
						else
						{
							ability.onKeyReleased();
						}
					}
				}
			});

			return null;
		}

	}

}
