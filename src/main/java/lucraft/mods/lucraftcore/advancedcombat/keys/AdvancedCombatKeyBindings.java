package lucraft.mods.lucraftcore.advancedcombat.keys;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.advancedcombat.ContestHandler;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.CapabilityAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.IAdvancedCombatCapability;
import lucraft.mods.lucraftcore.advancedcombat.gui.GuiCombatSelect;
import lucraft.mods.lucraftcore.advancedcombat.network.MessageSendContestTotal;
import lucraft.mods.lucraftcore.advancedcombat.network.MessageToggleCombat;
import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.events.AbilityKeyEvent;
import lucraft.mods.lucraftcore.superpowers.network.MessageAbilityKey;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.settings.KeyConflictContext;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

public class AdvancedCombatKeyBindings
{

	public static final KeyBinding COMBAT_MODE = new KeyBinding("lucraftcore.keybinding.keyCombatMode", KeyConflictContext.IN_GAME, Keyboard.KEY_COMMA,
			LucraftCore.NAME);
	public static final KeyBinding CONTEST = new KeyBinding("lucraftcore.keybinding.keyContest", KeyConflictContext.IN_GAME, Keyboard.KEY_M, LucraftCore.NAME);

	private static int iconTimer = 0;
	//TODO make textures
	private static final ResourceLocation combatOnTexture = new ResourceLocation(LucraftCore.MODID, "textures/gui/combat_off.png");
	private static final ResourceLocation combatOffTexture = new ResourceLocation(LucraftCore.MODID, "textures/gui/combat_on.png");

	private boolean leftClick, rightClick, inventory, drop;

	public AdvancedCombatKeyBindings()
	{
		ClientRegistry.registerKeyBinding(COMBAT_MODE);
		ClientRegistry.registerKeyBinding(CONTEST);
	}

	@SubscribeEvent(priority = EventPriority.NORMAL, receiveCanceled = true)
	public void onKey(InputEvent.KeyInputEvent evt)
	{
		if (COMBAT_MODE.isPressed())
		{
			if(Minecraft.getMinecraft().player.isSneaking())
				Minecraft.getMinecraft().displayGuiScreen(new GuiCombatSelect());
			else
			{
				LCPacketDispatcher.sendToServer(new MessageToggleCombat());
				IAdvancedCombatCapability cap = Minecraft.getMinecraft().player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
				cap.setCombatModeEnabled(!cap.isCombatModeEnabled());
				iconTimer = 20;
			}
		}
		if (CONTEST.isPressed())
		{
			if (ContestHandler.pressedAmount > 0)
			{
				if(ContestHandler.pressedAmount == 1){
					LCPacketDispatcher.sendToServer(new MessageSendContestTotal(ContestHandler.pressedAmount));
				}
				ContestHandler.pressedAmount++;
			}
		}
	}

	@SubscribeEvent(receiveCanceled = true)
	public void onInput(InputEvent event)
	{
		GameSettings s = Minecraft.getMinecraft().gameSettings;
		if (Minecraft.getMinecraft().player != null && Minecraft.getMinecraft().player.hasCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null)
				&& Minecraft.getMinecraft().player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null).isCombatModeEnabled())
		{
			IAdvancedCombatCapability cap = Minecraft.getMinecraft().player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
			boolean leftClick = isDown(s.keyBindAttack);
			boolean rightClick = isDown(s.keyBindUseItem);
			boolean dropKey = isDown(s.keyBindDrop);
			boolean invKey = isDown(s.keyBindInventory);

			if (s.keyBindAttack.isPressed())
				KeyBinding.setKeyBindState(s.keyBindAttack.getKeyCode(), false);
			if (s.keyBindUseItem.isPressed())
				KeyBinding.setKeyBindState(s.keyBindUseItem.getKeyCode(), false);
			if (s.keyBindDrop.isPressed())
				KeyBinding.setKeyBindState(s.keyBindDrop.getKeyCode(), false);
			if (s.keyBindInventory.isPressed())
				KeyBinding.setKeyBindState(s.keyBindInventory.getKeyCode(), false);

			//Send ac_keys to server
			if (this.leftClick != leftClick)
			{
				this.leftClick = leftClick;
				Ability ability = cap.getCombatBarAbilities()[0];
				if (ability == null || MinecraftForge.EVENT_BUS.post(new AbilityKeyEvent.Client(ability, true)))
					return;
				LCPacketDispatcher.sendToServer(new MessageAbilityKey(this.leftClick, -4, ability.context));
			}
			if (this.rightClick != rightClick)
			{
				this.rightClick = rightClick;
				Ability ability = cap.getCombatBarAbilities()[1];
				if (ability == null || MinecraftForge.EVENT_BUS.post(new AbilityKeyEvent.Client(ability, true)))
					return;
				LCPacketDispatcher.sendToServer(new MessageAbilityKey(this.rightClick, -3, ability.context));

			}
			if (this.drop != dropKey)
			{
				this.drop = dropKey;
				Ability ability = cap.getCombatBarAbilities()[2];
				if (ability == null || MinecraftForge.EVENT_BUS.post(new AbilityKeyEvent.Client(ability, true)))
					return;
				LCPacketDispatcher.sendToServer(new MessageAbilityKey(this.drop, -2, ability.context));

			}
			if (this.inventory != invKey)
			{
				this.inventory = invKey;
				Ability ability = cap.getCombatBarAbilities()[3];
				if (ability == null || MinecraftForge.EVENT_BUS.post(new AbilityKeyEvent.Client(ability, true)))
					return;
				LCPacketDispatcher.sendToServer(new MessageAbilityKey(this.inventory, -1, ability.context));
			}
		} else {
			leftClick = rightClick = drop = inventory = false;
		}
	}

	private boolean isDown(KeyBinding key){
		return (key.getKeyCode() > 0) ? Keyboard.isKeyDown(key.getKeyCode()) : Mouse.isButtonDown(key.getKeyCode() + 100);
	}

	@SubscribeEvent
	public void onClientTick(TickEvent.ClientTickEvent event)
	{
		if (event.phase == TickEvent.Phase.END && iconTimer > 0)
			iconTimer--;
	}

	@SubscribeEvent
	public void onRenderOverlay(RenderGameOverlayEvent.Post event)
	{
		if (event.getType() != RenderGameOverlayEvent.ElementType.ALL || iconTimer == 0)
			return;

		Minecraft mc = Minecraft.getMinecraft();
		ScaledResolution res = event.getResolution();
		int x = res.getScaledWidth() / 2 - 16;
		int y = res.getScaledHeight() - 100;
		boolean knockOut = mc.player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null).isCombatModeEnabled();
		float alpha = (iconTimer + event.getPartialTicks()) / 10F;

		GlStateManager.pushMatrix();
		GlStateManager.enableBlend();
		GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GlStateManager.color(1F, 1F, 1F, alpha);
		mc.renderEngine.bindTexture(knockOut ? combatOffTexture : combatOnTexture);
		Gui.drawModalRectWithCustomSizedTexture(x, y, 0, 0, 32, 32, 32, 32);
		GlStateManager.popMatrix();
	}

}
