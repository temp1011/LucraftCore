package lucraft.mods.lucraftcore.advancedcombat;

import lucraft.mods.lucraftcore.advancedcombat.capabilities.CapabilityAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.IAdvancedCombatCapability;
import lucraft.mods.lucraftcore.advancedcombat.keys.AdvancedCombatKeyBindings;
import lucraft.mods.lucraftcore.advancedcombat.network.MessageRequestContestTotal;
import lucraft.mods.lucraftcore.karma.KarmaHandler;
import lucraft.mods.lucraftcore.karma.KarmaStat;
import lucraft.mods.lucraftcore.karma.events.EntityKnockOutEvent;
import lucraft.mods.lucraftcore.karma.potions.PotionKnockOut;
import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import lucraft.mods.lucraftcore.util.network.MessageSyncPotionEffects;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraftforge.common.MinecraftForge;

import java.util.ArrayList;

public class ContestHandler
{
	public static int pressedAmount = 0;

	public static ArrayList<Contest> contests = new ArrayList<>();

	public static void onUpdate()
	{
		for (Contest contest : new ArrayList<>(contests))
		{
			if (contest.onUpdate())
			{
				contests.remove(contest);
			}
		}
	}

	public static void onClientUpdate(IAdvancedCombatCapability cap)
	{
		if (cap.getGrabee() == null && cap.getGrabber() == null)
		{
			pressedAmount = 0;
		}
		else if (pressedAmount == 0)
		{
			pressedAmount++;
		}

		if (cap.getGrabber() != null)
		{
			cap.getPlayer().sendStatusMessage(new TextComponentTranslation("lucraftcore.advancedcombat.you_are_grabbed", GameSettings.getKeyDisplayString(
					AdvancedCombatKeyBindings.CONTEST.getKeyCode())), true);
		}
		if (cap.getGrabee() != null)
		{
			cap.getPlayer().sendStatusMessage(new TextComponentTranslation("lucraftcore.advancedcombat.you_are_grabbing", GameSettings.getKeyDisplayString(
					AdvancedCombatKeyBindings.CONTEST.getKeyCode())), true);
		}
	}

	public static class Contest
	{
		private int ticks = 0;
		public EntityLivingBase grabber;
		public EntityLivingBase grabee;

		private int grabberTotal = -1;
		private int grabeeTotal = -1;

		private boolean sneakAttack;

		public Contest(EntityLivingBase grabber, EntityLivingBase grabee, boolean sneakAttack)
		{
			this.grabber = grabber;
			this.grabee = grabee;
			this.sneakAttack = sneakAttack;
		}

		public boolean onUpdate()
		{
			if(ticks > 0)
				ticks++;

			if(grabee == null || grabee.isDead || grabber == null || grabber.isDead)
				return true;

			if (ticks == 60)
			{
				if (grabber instanceof EntityPlayerMP)
					LCPacketDispatcher.sendTo(new MessageRequestContestTotal(), (EntityPlayerMP) grabber);
				if (grabee instanceof EntityPlayerMP)
					LCPacketDispatcher.sendTo(new MessageRequestContestTotal(), (EntityPlayerMP) grabee);
				if(!(grabee instanceof EntityPlayer && grabeeTotal == -1)){
					if (this.grabee instanceof EntityMob)
						grabeeTotal = 10;
					else
						grabeeTotal = 0;
				}
			}
			else if (ticks > 60 && grabeeTotal != -1 && grabberTotal != -1)
			{

				if (getWinner() == grabber)
				{
					if (!(grabber instanceof EntityPlayer) || !MinecraftForge.EVENT_BUS.post(new EntityKnockOutEvent.Start(grabee, (EntityPlayer) grabber)))
					{
						if (grabber instanceof EntityPlayer)
						{
							if (KarmaHandler.isEvilEntity(grabee))
								KarmaHandler.increaseKarmaStat((EntityPlayer) grabber, KarmaStat.BAD_KNOCKOUT);
							else if(KarmaHandler.isGoodEntity(grabee))
								KarmaHandler.increaseKarmaStat((EntityPlayer) grabber, KarmaStat.GOOD_KNOCKOUT);
						}

						grabee.setHealth(grabee.getMaxHealth());
						int duration = 60 * 20;
						PotionEffect blindness = new PotionEffect(MobEffects.BLINDNESS, duration, 0, false, false);
						blindness.setCurativeItems(new ArrayList<>());
						PotionEffect slowness = new PotionEffect(MobEffects.SLOWNESS, duration, 255, false, false);
						slowness.setCurativeItems(new ArrayList<>());
						PotionEffect jumpboost = new PotionEffect(MobEffects.JUMP_BOOST, duration, 128, false, false);
						jumpboost.setCurativeItems(new ArrayList<>());
						PotionEffect fatigue = new PotionEffect(MobEffects.MINING_FATIGUE, duration, 128, false, false);
						fatigue.setCurativeItems(new ArrayList<>());
						PotionEffect weakness = new PotionEffect(MobEffects.WEAKNESS, duration, 128, false, false);
						weakness.setCurativeItems(new ArrayList<>());
						PotionEffect knockout = new PotionEffect(PotionKnockOut.POTION_KNOCK_OUT, duration, 0, false, false);
						knockout.setCurativeItems(new ArrayList<>());

						grabee.addPotionEffect(blindness);
						grabee.addPotionEffect(slowness);
						grabee.addPotionEffect(jumpboost);
						grabee.addPotionEffect(fatigue);
						grabee.addPotionEffect(weakness);
						grabee.addPotionEffect(knockout);

						LCPacketDispatcher.sendToAll(new MessageSyncPotionEffects(grabee));
					}
				}
				else
				{
					if (grabee instanceof EntityPlayer)
						grabber.attackEntityFrom(DamageSource.causePlayerDamage((EntityPlayer) grabee), 2);
					else
						grabber.attackEntityFrom(DamageSource.causeMobDamage(grabee), 2);
				}
				if (grabber instanceof EntityPlayerMP)
				{
					IAdvancedCombatCapability cap = grabber.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
					cap.setGrabee(null);
					cap.syncToAll();
				}
				if (grabee instanceof EntityPlayerMP)
				{
					IAdvancedCombatCapability cap = grabee.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
					cap.setGrabber(null);
					cap.syncToAll();
				}
				return true;
			}
			return false;
		}

		EntityLivingBase getWinner()
		{
			int grabberScore = grabberTotal;
			int grabeeScore = grabeeTotal;

			if (sneakAttack)
				grabberScore += 10;

			int grabberStamina = (int) grabber.getHealth();
			if (grabber instanceof EntityPlayer)
				grabberStamina += ((EntityPlayer) grabber).getFoodStats().getFoodLevel() / 5;

			int grabeeStamina = (int) grabee.getHealth();
			if (grabee instanceof EntityPlayer)
				grabeeStamina += ((EntityPlayer) grabee).getFoodStats().getFoodLevel() / 5;

			if (grabberStamina > grabeeStamina)
				grabberScore += 5;
			else
				grabeeScore += 5;

			if (grabberScore > grabeeScore)
				return grabber;
			else
				return grabee;
		}
	}

	public static void onTotalMessage(EntityPlayer player, int total)
	{
		for (Contest contest : contests)
		{
			if ((contest.grabber == player || contest.grabee == player) && contest.ticks < 60){
				contest.ticks++;
			}
			if (contest.grabber == player)
			{
				contest.grabberTotal = total;
			}
			if (contest.grabee == player)
			{
				contest.grabeeTotal = total;
			}
		}
	}

}
