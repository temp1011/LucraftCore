package lucraft.mods.lucraftcore.advancedcombat;

import lucraft.mods.lucraftcore.advancedcombat.capabilities.CapabilityAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.IAdvancedCombatCapability;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public class AdvancedCombatEventHandler
{
	@SubscribeEvent
	public void onItemPickup(EntityItemPickupEvent event)
	{
		if (event.getEntityPlayer().hasCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null))
		{
			IAdvancedCombatCapability cap = event.getEntityPlayer().getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
			if (cap.isCombatModeEnabled())
			{
				InventoryPlayer inventory = event.getEntityPlayer().inventory;
				if (inventory.getStackInSlot(inventory.currentItem).isEmpty())
				{
					event.setCanceled(true);
					inventory.addItemStackToInventory(event.getItem().getItem());
					if (!inventory.getStackInSlot(inventory.currentItem).isEmpty())
					{
						int index = inventory.mainInventory.indexOf(ItemStack.EMPTY);
						if (index != -1)
						{
							inventory.setInventorySlotContents(index, inventory.getStackInSlot(inventory.currentItem));
							event.getItem().setDead();
							inventory.markDirty();
							event.getEntityPlayer().setHeldItem(EnumHand.MAIN_HAND, ItemStack.EMPTY);
						}
						else
							event.getEntityPlayer().dropItem(true);
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onTick(TickEvent.ServerTickEvent event)
	{
		if (event.phase != TickEvent.Phase.END)
			return;
		ContestHandler.onUpdate();
	}

	@SubscribeEvent
	public void onTick(LivingEvent.LivingUpdateEvent event)
	{
		for (ContestHandler.Contest contest : ContestHandler.contests)
		{
			if (contest.grabee == event.getEntityLiving())
			{
				if (!(contest.grabee instanceof EntityPlayer))
				{
					contest.grabee.motionX = 0;
					contest.grabee.motionY = 0;
					contest.grabee.motionZ = 0;
					Vec3d newPos = contest.grabber.getPositionVector().add(contest.grabber.getLookVec().normalize());
					contest.grabee.setLocationAndAngles(newPos.x, newPos.y + 0.5, newPos.z, contest.grabee.rotationYaw, contest.grabee.rotationPitch);
				}
			}
		}
	}

	@SubscribeEvent
	public void onAttack(AttackEntityEvent event)
	{
		IAdvancedCombatCapability cap = event.getEntityPlayer().getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
		if (cap != null)
			cap.resetCombatTimer();
	}

	@SubscribeEvent
	public void onAttacked(LivingAttackEvent event)
	{
		IAdvancedCombatCapability cap = event.getEntity().getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
		if (cap != null)
			cap.resetCombatTimer();
	}

}
