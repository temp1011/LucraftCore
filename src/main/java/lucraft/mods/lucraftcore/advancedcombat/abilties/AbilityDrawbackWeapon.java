package lucraft.mods.lucraftcore.advancedcombat.abilties;

import lucraft.mods.lucraftcore.advancedcombat.capabilities.CapabilityAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.IAdvancedCombatCapability;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityHeld;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.block.model.ItemCameraTransforms;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EnumCreatureAttribute;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.client.event.RenderSpecificHandEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AbilityDrawbackWeapon extends AbilityHeld
{
	public AbilityDrawbackWeapon(EntityPlayer player)
	{
		super(player);
	}

	@Override public void updateTick()
	{

	}

	@Override public void lastTick()
	{
		RayTraceResult rayTrace = PlayerHelper.rayTrace(player, player.getEntityAttribute(EntityPlayer.REACH_DISTANCE).getAttributeValue());
		double drawback = Math.min(30, this.ticks);
		double percent = drawback / 30;
		if (rayTrace.entityHit != null)
		{
			IAdvancedCombatCapability cap = player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
			ItemStack leftStack = player.getHeldItem(player.getPrimaryHand() == EnumHandSide.LEFT ? EnumHand.MAIN_HAND : EnumHand.OFF_HAND);
			ItemStack rightStack = player.getHeldItem(player.getPrimaryHand() == EnumHandSide.RIGHT ? EnumHand.MAIN_HAND : EnumHand.OFF_HAND);
			ItemStack abStack = ItemStack.EMPTY;

			Ability[] barAbilities = cap.getCombatBarAbilities();
			for (int i = 0; i < barAbilities.length; i++)
			{
				if(i == 0 && barAbilities[i] == this){
					abStack = leftStack;
				} else if(i == 1 && barAbilities[i] == this){
					abStack = rightStack;
				}
			}

			if(!abStack.isEmpty())
			{
				float f1;
				if (rayTrace.entityHit instanceof EntityLivingBase)
				{
					f1 = EnchantmentHelper.getModifierForCreature(abStack, ((EntityLivingBase) rayTrace.entityHit).getCreatureAttribute());
				}
				else
				{
					f1 = EnchantmentHelper.getModifierForCreature(abStack, EnumCreatureAttribute.UNDEFINED);
				}
				rayTrace.entityHit.attackEntityFrom(DamageSource.causePlayerDamage(player),
						(float) ((player.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).getAttributeValue() + 2f + f1) * percent));
			}
		}
	}

	@SideOnly(Side.CLIENT)
	@Override public void renderLeftHand(RenderSpecificHandEvent event)
	{
		GlStateManager.pushMatrix();
		EntityPlayerSP clientPlayer = (EntityPlayerSP) player;

		ItemStack heldStack = player.getHeldItem(player.getPrimaryHand() == EnumHandSide.LEFT ? EnumHand.MAIN_HAND : EnumHand.OFF_HAND);
		if(heldStack.isEmpty()) return;

		int i = -1;
		float drawback = Math.min(30, this.ticks);

		GlStateManager.translate((float) i * 0.56F, -0.52F, -0.72F);
		GlStateManager.rotate(-30 + drawback, 1.0f, 0.0f, 0.0f);

		if (Minecraft.getMinecraft().getRenderItem().shouldRenderItemIn3D(heldStack)
				&& Block.getBlockFromItem(heldStack.getItem()).getBlockLayer() == BlockRenderLayer.TRANSLUCENT)
		{
			GlStateManager.depthMask(false);
		}
		Minecraft.getMinecraft().getRenderItem().renderItem(heldStack, clientPlayer, ItemCameraTransforms.TransformType.FIRST_PERSON_LEFT_HAND, true);
		GlStateManager.depthMask(true);
		GlStateManager.popMatrix();
	}

	@SideOnly(Side.CLIENT)
	@Override public void renderRightHand(RenderSpecificHandEvent event)
	{
		GlStateManager.pushMatrix();
		EntityPlayerSP clientPlayer = (EntityPlayerSP) player;

		ItemStack heldStack = player.getHeldItem(player.getPrimaryHand() == EnumHandSide.RIGHT ? EnumHand.MAIN_HAND : EnumHand.OFF_HAND);
		if(heldStack.isEmpty()) return;

		int i = 1;
		float drawback = Math.min(30, this.ticks);

		GlStateManager.translate((float) i * 0.56F, -0.52F, -0.72F);
		GlStateManager.rotate(-30 + drawback, 1.0f, 0.0f, 0.0f);

		if (Minecraft.getMinecraft().getRenderItem().shouldRenderItemIn3D(heldStack)
				&& Block.getBlockFromItem(heldStack.getItem()).getBlockLayer() == BlockRenderLayer.TRANSLUCENT)
		{
			GlStateManager.depthMask(false);
		}
		Minecraft.getMinecraft().getRenderItem().renderItem(heldStack, clientPlayer, ItemCameraTransforms.TransformType.FIRST_PERSON_RIGHT_HAND, false);
		GlStateManager.depthMask(true);
		GlStateManager.popMatrix();
	}

	public boolean isValidForCombatBarSlot(int combatBarIndex)
	{
		if(getJsonID().contains("left")) return combatBarIndex == 0;
		if(getJsonID().contains("right")) return combatBarIndex == 1;
		return combatBarIndex == 0 || combatBarIndex == 1;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawIcon(Minecraft mc, Gui gui, int x, int y)
	{
		IAdvancedCombatCapability cap = player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
		ItemStack leftStack = player.getHeldItem(player.getPrimaryHand() == EnumHandSide.LEFT ? EnumHand.MAIN_HAND : EnumHand.OFF_HAND);
		ItemStack rightStack = player.getHeldItem(player.getPrimaryHand() == EnumHandSide.RIGHT ? EnumHand.MAIN_HAND : EnumHand.OFF_HAND);

		Ability[] barAbilities = cap.getCombatBarAbilities();
		for (int i = 0; i < barAbilities.length; i++)
		{
			if(i == 0 && barAbilities[i] == this){
				mc.getRenderItem().renderItemIntoGUI(!cap.isCombatModeEnabled() || leftStack.isEmpty() ? new ItemStack(Items.IRON_SWORD) : leftStack, x, y);
				return;
			} else if(i == 1 && barAbilities[i] == this){
				mc.getRenderItem().renderItemIntoGUI(!cap.isCombatModeEnabled() || rightStack.isEmpty() ? new ItemStack(Items.IRON_SWORD) : rightStack, x, y);
				return;
			}
		}
	}
}
