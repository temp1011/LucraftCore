package lucraft.mods.lucraftcore.advancedcombat.abilties;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.advancedcombat.ModuleAdvancedCombat;
import lucraft.mods.lucraftcore.superpowers.ModuleSuperpowers;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityAction;
import lucraft.mods.lucraftcore.util.events.RenderModelEvent;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelPlayer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.WorldServer;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.ArrayList;

public class AbilitySweepKick extends AbilityAction
{
	private int kickTime;

	public AbilitySweepKick(EntityPlayer player)
	{
		super(player);
		this.setMaxCooldown(10);
	}

	@Override public void onUpdate()
	{
		super.onUpdate();
		if (player.isSneaking() && kickTime > 0)
		{
			kickTime++;
			if (kickTime == 5)
			{
				double reach = player.getEntityAttribute(EntityPlayer.REACH_DISTANCE).getAttributeValue() - 1f;
				AxisAlignedBB box = player.getEntityBoundingBox().grow(reach, reach, reach);
				ArrayList<EntityLivingBase> list = (ArrayList<EntityLivingBase>) player.world.getEntitiesWithinAABB(EntityLivingBase.class, box);
				for (EntityLivingBase e : list)
				{
					if (e != player && e.getDistance(player) <= reach)
					{
						Vec3d vec3d = e.getPositionVector();
						Vec3d vec3d1 = player.getLook(1.0F);
						Vec3d vec3d2 = vec3d.subtractReverse(new Vec3d(player.posX, player.posY, player.posZ)).normalize();
						vec3d2 = new Vec3d(vec3d2.x, 0.0D, vec3d2.z);
						if (vec3d2.dotProduct(vec3d1) < 0.0D)
						{
							e.attackEntityFrom(DamageSource.causePlayerDamage(player),
									(float) player.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).getAttributeValue());
						}
					}
				}
				if (!player.world.isRemote)
				{
					Vec3d position = player.getPositionVector().add(player.getLookVec().normalize());
					((WorldServer) player.world)
							.spawnParticle(EnumParticleTypes.SWEEP_ATTACK, position.x, player.posY + 0.35, position.z, 1, 0.0, 0.0, 0.0, 0.0);
				}
			}
			else if (kickTime == 10)
				kickTime = 0;
		}
		else
		{
			kickTime = 0;
		}
	}

	@Override public boolean action()
	{
		if (player.isSneaking() && this.kickTime == 0)
		{
			kickTime = 1;
			return true;
		}
		return false;
	}

	@Override public NBTTagCompound serializeNBT()
	{
		NBTTagCompound nbt = super.serializeNBT();
		nbt.setInteger("kickTime", this.kickTime);
		return nbt;
	}

	@Override public void deserializeNBT(NBTTagCompound nbt)
	{
		super.deserializeNBT(nbt);
		this.kickTime = nbt.getInteger("kickTime");
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawIcon(Minecraft mc, Gui gui, int x, int y)
	{
		LCRenderHelper.drawIcon(mc, gui, x, y, 1, 4);
	}

	@Mod.EventBusSubscriber(modid = LucraftCore.MODID, value = Side.CLIENT)
	@SideOnly(Side.CLIENT)
	public static class Renderer
	{

		@SubscribeEvent(receiveCanceled = true)
		public static void onRenderModel(RenderModelEvent.SetRotationAngels event)
		{
			if (!ModuleSuperpowers.INSTANCE.isEnabled())
				return;

			if (event.getEntity() instanceof EntityPlayer)
			{
				for (AbilitySweepKick ab : Ability
						.getAbilitiesFromClass(Ability.getCurrentPlayerAbilities((EntityPlayer) event.getEntity()), AbilitySweepKick.class))
				{
					if (ab != null && ab.isUnlocked())
					{
						float renderKick = (ab.kickTime + LCRenderHelper.renderTick - 1) * 20f;
						if (ab.kickTime > 0)
						{
							event.model.bipedLeftLeg.rotateAngleX = -(float) Math.toRadians(-50);
							event.model.bipedLeftLeg.rotateAngleY = (float) Math.toRadians(270 - renderKick);
							event.model.bipedLeftLeg.rotateAngleZ = 0;
							if (event.model instanceof ModelPlayer)
							{
								((ModelPlayer) event.model).bipedLeftLegwear.rotateAngleX = -(float) Math.toRadians(-50);
								((ModelPlayer) event.model).bipedLeftLegwear.rotateAngleY = (float) Math.toRadians(270 - renderKick);
								((ModelPlayer) event.model).bipedLeftLegwear.rotateAngleZ = 0;
							}
							event.setCanceled(true);
						}
					}
				}
			}
		}

		@SubscribeEvent
		public static void onSetUpModel(RenderHandEvent event)
		{
			if (!ModuleAdvancedCombat.INSTANCE.isEnabled())
				return;
			EntityPlayer player = Minecraft.getMinecraft().player;
			for (AbilitySweepKick ab : Ability.getAbilitiesFromClass(Ability.getCurrentPlayerAbilities(player), AbilitySweepKick.class))
			{
				if (ab != null && ab.isUnlocked())
				{
					RenderLivingBase renderer = (RenderLivingBase) Minecraft.getMinecraft().getRenderManager().getEntityRenderObject(player);
					if (renderer != null && Minecraft.getMinecraft().gameSettings.thirdPersonView == 0)
					{
						if (ab.kickTime > 0)
						{
							ModelBiped model = (ModelBiped) renderer.getMainModel();
							model.setRotationAngles(0, 0, player.ticksExisted, 0f, 0f, 0.0625f, player);
							float renderKick = (ab.kickTime + event.getPartialTicks() - 1) * 12f;
							GlStateManager.pushMatrix();
							GlStateManager.translate(0.15F, -0.9F, -0.15F);
							GlStateManager.rotate(130.0F, 0.0F, 1.0F, 0.0F);
							GlStateManager.rotate(renderKick, 0.0F, 1.0F, 0.0F);
							GlStateManager.rotate(80.0F, 1.0F, 0.0F, 0.0F);
							Minecraft.getMinecraft().renderEngine.bindTexture(((AbstractClientPlayer) player).getLocationSkin());
							model.bipedLeftLeg.rotateAngleX = 0;
							model.bipedLeftLeg.rotateAngleY = 0;
							model.bipedLeftLeg.rotateAngleZ = 0;
							model.bipedLeftLeg.render(0.0625f);
							GlStateManager.popMatrix();
						}
					}
				}
			}
		}

	}
}
