package lucraft.mods.lucraftcore.advancedcombat.abilties;

import lucraft.mods.lucraftcore.LCConfig;
import lucraft.mods.lucraftcore.advancedcombat.ContestHandler;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.CapabilityAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.IAdvancedCombatCapability;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityAction;
import lucraft.mods.lucraftcore.util.helper.LCEntityHelper;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import noppes.npcs.api.NpcAPI;
import noppes.npcs.api.entity.ICustomNpc;

import java.util.ArrayList;

public class AbilityGrab extends AbilityAction
{
	public AbilityGrab(EntityPlayer player)
	{
		super(player);
	}

	public void onKeyPressed()
	{
		if(player.isSneaking()){
			IAdvancedCombatCapability cap = player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
			if (cap.getGrabee() == null)
				return;

			EntityLivingBase grabee = cap.getGrabee();

			if(grabee instanceof EntityPlayerMP){
				IAdvancedCombatCapability cap2 = grabee.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
				cap2.setGrabber(null);
				cap2.syncToAll();
			}
			for (ContestHandler.Contest contest : new ArrayList<>(ContestHandler.contests))
			{
				if(contest.grabber == player){
					ContestHandler.contests.remove(contest);
				}
			}
			cap.setGrabee(null);
			cap.syncToAll();
			return;
		}
		super.onKeyPressed();
	}

	@Override public boolean action()
	{
		if(!isValid())
			return false;
		RayTraceResult rayTrace = PlayerHelper.rayTrace(player, player.getEntityAttribute(EntityPlayer.REACH_DISTANCE).getAttributeValue());
		if (rayTrace.entityHit instanceof EntityLivingBase)
		{
			if(NpcAPI.IsAvailable() && NpcAPI.Instance().getIEntity(rayTrace.entityHit) != null && NpcAPI.Instance().getIEntity(rayTrace.entityHit) instanceof ICustomNpc && ((ICustomNpc) NpcAPI.Instance().getIEntity(rayTrace.entityHit)).getRole() != null)
				return false;
			for (String disabledGrabEntity : LCConfig.ac.disabledGrabEntities)
			{
				ResourceLocation mob = EntityList.getKey(rayTrace.entityHit);
				if(mob != null && mob.toString().equals(disabledGrabEntity))
					return false;
			}
			IAdvancedCombatCapability cap = player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
			if (cap.getGrabber() != null || cap.getGrabee() != null)
				return false;

			boolean sneakAttack = false;
			if(cap.getCombatTimer() <= 0){
				sneakAttack = !LCEntityHelper.isInFrontOfEntity(rayTrace.entityHit, player);
			}
			ContestHandler.contests.add(new ContestHandler.Contest(player, (EntityLivingBase) rayTrace.entityHit, sneakAttack));
			cap.setGrabee((EntityLivingBase) rayTrace.entityHit);
			cap.syncToAll();
			if (rayTrace.entityHit instanceof EntityPlayer)
			{
				IAdvancedCombatCapability cap2 = rayTrace.entityHit.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
				cap2.setGrabber(player);
				cap2.syncToAll();
			}
			return true;
		}
		return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		if(!isValid())
			GlStateManager.color(0.45f, 0.45f, 0.45f);
		LCRenderHelper.drawIcon(mc, gui, x, y, 1, 5);
		GlStateManager.color(1.0f, 1.0f, 1.0f);
	}

	public boolean isValid(){
		return (player.getHeldItemMainhand().isEmpty() && player.getHeldItemOffhand().isEmpty());
	}
}
