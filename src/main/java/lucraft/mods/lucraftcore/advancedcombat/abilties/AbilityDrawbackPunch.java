package lucraft.mods.lucraftcore.advancedcombat.abilties;

import lucraft.mods.lucraftcore.advancedcombat.ModuleAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.CapabilityAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.IAdvancedCombatCapability;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityHeld;
import lucraft.mods.lucraftcore.util.events.RenderModelEvent;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelPlayer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EnumPlayerModelParts;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.client.event.RenderSpecificHandEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AbilityDrawbackPunch extends AbilityHeld
{
	public AbilityDrawbackPunch(EntityPlayer player)
	{
		super(player);
	}

	@Override public void updateTick()
	{

	}

	@Override public void lastTick()
	{
		RayTraceResult rayTrace = PlayerHelper.rayTrace(player, player.getEntityAttribute(EntityPlayer.REACH_DISTANCE).getAttributeValue());
		double drawback = Math.min(30, this.ticks);
		double percent = drawback / 30;
		if (rayTrace.entityHit != null)
		{
			rayTrace.entityHit.attackEntityFrom(DamageSource.causePlayerDamage(player),
					(float) ((player.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).getAttributeValue() + 2f) * percent));
		}
	}

	@SideOnly(Side.CLIENT)
	@Override public void renderLeftHand(RenderSpecificHandEvent event)
	{
		GlStateManager.pushMatrix();
		EntityPlayerSP clientPlayer = (EntityPlayerSP) player;

		float swingProgress = 0;

		float f = -1.0F;
		float f1 = MathHelper.sqrt(swingProgress);
		float f2 = -0.3F * MathHelper.sin(f1 * 3.1415927F);
		float f3 = 0.4F * MathHelper.sin(f1 * 6.2831855F);
		float f4 = -0.4F * MathHelper.sin(swingProgress * 3.1415927F);
		GlStateManager.translate(f * (f2 + 0.64000005F), f3 + -0.6F + swingProgress * -0.6F, f4 + -0.71999997F);
		GlStateManager.rotate(f * 45.0F, 0.0F, 1.0F, 0.0F);
		float f5 = MathHelper.sin(swingProgress * swingProgress * 3.1415927F);
		float f6 = MathHelper.sin(f1 * 3.1415927F);
		GlStateManager.rotate(f * f6 * 70.0F, 0.0F, 1.0F, 0.0F);
		GlStateManager.rotate(f * f5 * -20.0F, 0.0F, 0.0F, 1.0F);
		Minecraft.getMinecraft().getTextureManager().bindTexture(clientPlayer.getLocationSkin());
		GlStateManager.translate(f * -1.0F, 3.6F, 3.5F);
		GlStateManager.rotate(f * 120.0F, 0.0F, 0.0F, 1.0F);
		GlStateManager.rotate(200.0F, 1.0F, 0.0F, 0.0F);
		GlStateManager.rotate(f * -135.0F, 0.0F, 1.0F, 0.0F);
		GlStateManager.translate(f * 5.6F, 0.0F, 0.0F);
		RenderLivingBase renderlb = (RenderLivingBase) Minecraft.getMinecraft().getRenderManager().getEntityRenderObject(clientPlayer);
		if(renderlb instanceof RenderPlayer)
		{
			RenderPlayer renderPlayer = (RenderPlayer) renderlb;
			GlStateManager.disableCull();
			{
				GlStateManager.color(1.0F, 1.0F, 1.0F);
				ModelPlayer modelplayer = renderPlayer.getMainModel();
				modelplayer.bipedLeftArmwear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.LEFT_SLEEVE);
				modelplayer.leftArmPose = ModelBiped.ArmPose.EMPTY;

				GlStateManager.enableBlend();
				modelplayer.isSneak = false;
				modelplayer.swingProgress = 0.0F;
				modelplayer.setRotationAngles(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F, clientPlayer);

				int drawback = Math.min(30, this.ticks);
				GlStateManager.translate(0.25 - drawback/-75f,  0.2 + drawback/-75f, -0.1 + drawback/-300f);
				modelplayer.bipedLeftArm.rotateAngleX = modelplayer.bipedLeftArmwear.rotateAngleX = 0.3F;
				modelplayer.bipedLeftArm.rotateAngleY = modelplayer.bipedLeftArmwear.rotateAngleY = -1.4F;
				modelplayer.bipedLeftArm.rotateAngleZ = modelplayer.bipedLeftArmwear.rotateAngleZ = 0.2F;

				modelplayer.bipedLeftArm.render(0.0625F);
				modelplayer.bipedLeftArmwear.render(0.0625F);
				GlStateManager.disableBlend();
			}
		}

		GlStateManager.enableCull();
		GlStateManager.popMatrix();
	}

	@SideOnly(Side.CLIENT)
	@Override public void renderRightHand(RenderSpecificHandEvent event)
	{
		GlStateManager.pushMatrix();
		EntityPlayerSP clientPlayer = (EntityPlayerSP) player;

		float swingProgress = 0;

		float f = 1.0F;
		float f1 = MathHelper.sqrt(swingProgress);
		float f2 = -0.3F * MathHelper.sin(f1 * 3.1415927F);
		float f3 = 0.4F * MathHelper.sin(f1 * 6.2831855F);
		float f4 = -0.4F * MathHelper.sin(swingProgress * 3.1415927F);
		GlStateManager.translate(f * (f2 + 0.54000005F), f3 + -0.6F + swingProgress * -0.6F, f4 + -0.71999997F);
		GlStateManager.rotate(f * 45.0F, 0.0F, 1.0F, 0.0F);
		float f5 = MathHelper.sin(swingProgress * swingProgress * 3.1415927F);
		float f6 = MathHelper.sin(f1 * 3.1415927F);
		GlStateManager.rotate(f * f6 * 70.0F, 0.0F, 1.0F, 0.0F);
		GlStateManager.rotate(f * f5 * -20.0F, 0.0F, 0.0F, 1.0F);
		Minecraft.getMinecraft().getTextureManager().bindTexture(clientPlayer.getLocationSkin());
		GlStateManager.translate(f * -1.0F, 3.6F, 3.5F);
		GlStateManager.rotate(f * 120.0F, 0.0F, 0.0F, 1.0F);
		GlStateManager.rotate(200.0F, 1.0F, 0.0F, 0.0F);
		GlStateManager.rotate(f * -135.0F, 0.0F, 1.0F, 0.0F);
		GlStateManager.translate(f * 5.6F, 0.0F, 0.0F);
		RenderLivingBase renderlb = (RenderLivingBase) Minecraft.getMinecraft().getRenderManager().getEntityRenderObject(clientPlayer);
		if(renderlb instanceof RenderPlayer)
		{
			RenderPlayer renderPlayer = (RenderPlayer) renderlb;
			GlStateManager.disableCull();
			{
				GlStateManager.color(1.0F, 1.0F, 1.0F);
				ModelPlayer modelplayer = renderPlayer.getMainModel();
				modelplayer.bipedRightArmwear.showModel = clientPlayer.isWearing(EnumPlayerModelParts.RIGHT_SLEEVE);
				modelplayer.rightArmPose = ModelBiped.ArmPose.EMPTY;

				GlStateManager.enableBlend();
				modelplayer.isSneak = false;
				modelplayer.swingProgress = 0.0F;
				modelplayer.setRotationAngles(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F, clientPlayer);

				int drawback = Math.min(30, this.ticks);
				GlStateManager.translate(-0.25 - drawback/75f,  0.3 + drawback/-75f, 0.1 + drawback/-300f);
				modelplayer.bipedRightArm.rotateAngleX = modelplayer.bipedRightArmwear.rotateAngleX = -0.3F;
				modelplayer.bipedRightArm.rotateAngleY = modelplayer.bipedRightArmwear.rotateAngleY = -1.6F;
				modelplayer.bipedRightArm.rotateAngleZ = modelplayer.bipedRightArmwear.rotateAngleZ = -0.2F;

				modelplayer.bipedRightArm.render(0.0625F);
				modelplayer.bipedRightArmwear.render(0.0625F);
				GlStateManager.disableBlend();
			}
		}

		GlStateManager.enableCull();
		GlStateManager.popMatrix();
	}

	@SideOnly(Side.CLIENT)
	@Override public void onSetupModelInCombatBar(RenderModelEvent.SetRotationAngels event, int combatBarIndex)
	{
		if(player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null).getGrabee() != null) return;

		float pitch = player.prevRotationPitch + (player.rotationPitch - player.prevRotationPitch) * LCRenderHelper.renderTick;
		float renderYawOffset = player.prevRenderYawOffset + (player.renderYawOffset - player.prevRenderYawOffset) * LCRenderHelper.renderTick;
		float yaw = player.prevRotationYawHead + (player.rotationYawHead - player.prevRotationYawHead) * LCRenderHelper.renderTick;
		event.setCanceled(true);
		//TODO offsetting
		if (combatBarIndex == ModuleAdvancedCombat.LEFT_HAND)
		{
			event.model.bipedLeftArm.rotateAngleX = (float) Math.toRadians(pitch - 100);
			event.model.bipedLeftArm.rotateAngleY = (float) Math.toRadians(yaw - renderYawOffset + 10);
			event.model.bipedLeftArm.rotateAngleZ = 0;
			if (event.model instanceof ModelPlayer)
			{
				((ModelPlayer) event.model).bipedLeftArmwear.rotateAngleX = (float) Math.toRadians(pitch - 100);
				((ModelPlayer) event.model).bipedLeftArmwear.rotateAngleY = (float) Math.toRadians(yaw - renderYawOffset + 10);
				((ModelPlayer) event.model).bipedLeftArmwear.rotateAngleZ = 0f;
			}

		}
		else if (combatBarIndex == ModuleAdvancedCombat.RIGHT_HAND)
		{
			event.model.bipedRightArm.rotateAngleX = (float) Math.toRadians(pitch - 100);
			event.model.bipedRightArm.rotateAngleY = (float) Math.toRadians(yaw - renderYawOffset - 10);
			event.model.bipedRightArm.rotateAngleZ = 0;
			if (event.model instanceof ModelPlayer)
			{
				((ModelPlayer) event.model).bipedRightArmwear.rotateAngleX = (float) Math.toRadians(pitch - 100);
				((ModelPlayer) event.model).bipedRightArmwear.rotateAngleY = (float) Math.toRadians(yaw - renderYawOffset - 10);
				((ModelPlayer) event.model).bipedRightArmwear.rotateAngleZ = 0;

			}
		}
		//		LimbManipulationUtil.getLimbManipulator(event.getRenderer(), LimbManipulationUtil.Limb.LEFT_ARM)
		//				.setAngles(pitch - 100, yaw - renderYawOffset + 10, 0).setOffsets(0f, -drawback / 80f, 0f);
		//		LimbManipulationUtil.getLimbManipulator(event.getRenderer(), LimbManipulationUtil.Limb.RIGHT_ARM)
		//				.setAngles(pitch - 100, yaw - renderYawOffset - 10, 0).setOffsets(0f, -drawback / 80f, 0f);
	}

	public boolean isValidForCombatBarSlot(int combatBarIndex)
	{
		if(getJsonID().contains("left")) return combatBarIndex == 0;
		if(getJsonID().contains("right")) return combatBarIndex == 1;
		return combatBarIndex == 0 || combatBarIndex == 1;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		IAdvancedCombatCapability cap = mc.player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);

		for (int i = 0; i < cap.getCombatBarAbilities().length; i++)
		{
			if(i == 0 && cap.getCombatBarAbilities()[i] == this){
				LCRenderHelper.drawIcon(mc, gui, x, y, 1, 2);return;
			} else if(i == 1 && cap.getCombatBarAbilities()[i] == this){
				LCRenderHelper.drawIcon(mc, gui, x, y, 1, 3);return;
			}
		}

	}
}
