package lucraft.mods.lucraftcore.advancedcombat.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.CapabilityAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.IAdvancedCombatCapability;
import lucraft.mods.lucraftcore.network.AbstractServerMessageHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageToggleCombat implements IMessage {

	public MessageToggleCombat() {
	}

	@Override
	public void fromBytes(ByteBuf buf) {

	}

	@Override
	public void toBytes(ByteBuf buf) {

	}

	public static class Handler extends AbstractServerMessageHandler<MessageToggleCombat> {

		@Override
		public IMessage handleServerMessage(EntityPlayer player, MessageToggleCombat message, MessageContext ctx) {

			LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {
				IAdvancedCombatCapability cap = player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
				cap.setCombatModeEnabled(!cap.isCombatModeEnabled());
			});

			return null;
		}

	}

}
