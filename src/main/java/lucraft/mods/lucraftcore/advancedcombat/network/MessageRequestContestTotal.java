package lucraft.mods.lucraftcore.advancedcombat.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.advancedcombat.ContestHandler;
import lucraft.mods.lucraftcore.network.AbstractClientMessageHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageRequestContestTotal implements IMessage {


	public MessageRequestContestTotal() {
	}


	@Override
	public void fromBytes(ByteBuf buf) {
	}

	@Override
	public void toBytes(ByteBuf buf) {

	}

	public static class Handler extends AbstractClientMessageHandler<MessageRequestContestTotal>
	{

		@Override
		public IMessage handleClientMessage(EntityPlayer player, MessageRequestContestTotal message, MessageContext ctx) {
			int total = ContestHandler.pressedAmount;
			ContestHandler.pressedAmount = 0;
			return new MessageSendContestTotal(total);
		}

	}

}
