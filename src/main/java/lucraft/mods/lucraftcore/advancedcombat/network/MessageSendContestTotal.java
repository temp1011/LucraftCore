package lucraft.mods.lucraftcore.advancedcombat.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.advancedcombat.ContestHandler;
import lucraft.mods.lucraftcore.network.AbstractServerMessageHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSendContestTotal implements IMessage {


	private int total;

	public MessageSendContestTotal() {
	}

	public MessageSendContestTotal(int total) {
		this.total = total;
	}


	@Override
	public void fromBytes(ByteBuf buf) {
		this.total = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(this.total);
	}

	public static class Handler extends AbstractServerMessageHandler<MessageSendContestTotal>
	{

		@Override public IMessage handleServerMessage(EntityPlayer player, MessageSendContestTotal message, MessageContext ctx)
		{
			ContestHandler.onTotalMessage(player, message.total);
			return null;
		}
	}

}
