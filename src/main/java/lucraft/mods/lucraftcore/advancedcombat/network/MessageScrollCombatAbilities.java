package lucraft.mods.lucraftcore.advancedcombat.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.CapabilityAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.IAdvancedCombatCapability;
import lucraft.mods.lucraftcore.network.AbstractServerMessageHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageScrollCombatAbilities implements IMessage
{

	private int index;
	private boolean up;

	public MessageScrollCombatAbilities() {
	}

	public MessageScrollCombatAbilities(int index, boolean up) {
		this.index = index;
		this.up = up;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		this.index = buf.readInt();
		this.up = buf.readBoolean();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(index);
		buf.writeBoolean(up);
	}

	public static class Handler extends AbstractServerMessageHandler<MessageScrollCombatAbilities>
	{

		@Override
		public IMessage handleServerMessage(EntityPlayer player, MessageScrollCombatAbilities message, MessageContext ctx) {

			LucraftCore.proxy.getThreadFromContext(ctx).addScheduledTask(() -> {
				IAdvancedCombatCapability cap = player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
				if(message.up)
					cap.scrollUpCombatBar(message.index);
				else cap.scrollDownCombatBar(message.index);

				cap.syncToAll();
			});

			return null;
		}

	}

}
