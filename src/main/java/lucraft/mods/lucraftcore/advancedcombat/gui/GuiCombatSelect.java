package lucraft.mods.lucraftcore.advancedcombat.gui;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.CapabilityAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.IAdvancedCombatCapability;
import lucraft.mods.lucraftcore.advancedcombat.network.MessageScrollCombatAbilities;
import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilitybar.SuperpowerAbilityBarEntry;
import lucraft.mods.lucraftcore.util.abilitybar.IAbilityBarEntry;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.util.ResourceLocation;

import java.io.IOException;

public class GuiCombatSelect extends GuiScreen
{
	private static ResourceLocation texture = new ResourceLocation(LucraftCore.MODID, "textures/gui/combat_bar_select.png");

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		this.drawDefaultBackground();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		Minecraft.getMinecraft().renderEngine.bindTexture(texture);

		int i = (this.width - 174) / 2;
		int j = (this.height - 60) / 2;
		this.drawTexturedModalRect(i, j, 0, 0, 174, 60);

		GlStateManager.pushMatrix();

		GlStateManager.enableRescaleNormal();
		RenderHelper.enableGUIStandardItemLighting();


		IAbilityBarEntry[] combatBarEntries = new IAbilityBarEntry[4];
		IAdvancedCombatCapability cap = mc.player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);

		Ability[] combatBarAbilities = cap.getCombatBarAbilities();

		for (int n = 0; n < combatBarAbilities.length; n++)
		{
			if (combatBarAbilities[n] != null)
				combatBarEntries[n] = new SuperpowerAbilityBarEntry(combatBarAbilities[n], n);
		}

		for (int k = 0; k < combatBarEntries.length; k++)
		{
			Minecraft.getMinecraft().renderEngine.bindTexture(texture);
			GlStateManager.translate(0.5, 0, 0);
			GlStateManager.color(1, 1, 1, 1);

			this.drawTexturedModalRect(i + 9 + k * 44, j + 18, 0, 70, 22, 22);
			if (mouseX > i + 13 + k * 44 && mouseX < i + 13 + k * 44 + 14 && mouseY > j + 8 && mouseY < j + 8 + 9)
				GlStateManager.color(0.5f, 0.5f, 1.0f);
			else
				GlStateManager.color(1.0f, 1.0f, 1.0f);
			this.drawTexturedModalRect(i + 13 + k * 44, j + 8, 0, 60, 14, 9);
			if (mouseX > i + 13 + k * 44 && mouseX < i + 13 + k * 44 + 14 && mouseY > j + 41 && mouseY < j + 41 + 9)
				GlStateManager.color(0.5f, 0.5f, 1.0f);
			else
				GlStateManager.color(1.0f, 1.0f, 1.0f);
			this.drawTexturedModalRect(i + 13 + k * 44, j + 41, 14, 60, 14, 9);

			GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
			if (combatBarEntries[k] != null)
				combatBarEntries[k].drawIcon(mc, this, i + 12 + k * 44, j + 21);

			GlStateManager.translate(-0.5, 0, 0);
		}

		RenderHelper.disableStandardItemLighting();
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();

		super.drawScreen(mouseX, mouseY, partialTicks);
	}

	@Override protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
	{
		int i = (this.width - 174) / 2;
		int j = (this.height - 60) / 2;

		super.mouseClicked(mouseX, mouseY, mouseButton);

		for (int k = 0; k < 4; k++)
		{
			if (mouseX > i + 13 + k * 44 && mouseX < i + 13 + k * 44 + 14 && mouseY > j + 8 && mouseY < j + 8 + 9)
			{
				LCPacketDispatcher.sendToServer(new MessageScrollCombatAbilities(k, true));
			}
			else if (mouseX > i + 13 + k * 44 && mouseX < i + 13 + k * 44 + 14 && mouseY > j + 41 && mouseY < j + 41 + 9)
			{
				LCPacketDispatcher.sendToServer(new MessageScrollCombatAbilities(k, false));
			}
		}
	}
}
