package lucraft.mods.lucraftcore.advancedcombat;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.CapabilityAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.IAdvancedCombatCapability;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilitybar.SuperpowerAbilityBarEntry;
import lucraft.mods.lucraftcore.superpowers.render.SuperpowerRenderLayer;
import lucraft.mods.lucraftcore.util.abilitybar.IAbilityBarEntry;
import lucraft.mods.lucraftcore.util.events.RenderModelEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelPlayer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.client.event.RenderSpecificHandEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.lwjgl.opengl.GL11;

public class AdvancedCombatClientEventHandler
{
	@SubscribeEvent
	public void onRenderSpecificHand(RenderSpecificHandEvent event)
	{
		IAdvancedCombatCapability cap = Minecraft.getMinecraft().player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
		if (cap == null)
			return;
		if (cap.isCombatModeEnabled())
		{
			event.setCanceled(true);

			Ability[] list = cap.getCombatBarAbilities();

			if (list[0] != null)
				list[0].renderLeftHand(event);
			if (list[1] != null)
				list[1].renderRightHand(event);
		}

	}

	@SubscribeEvent
	public void onRenderHand(RenderHandEvent event)
	{
		IAdvancedCombatCapability cap = Minecraft.getMinecraft().player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
		if (cap == null)
			return;
		if (cap.getGrabee() != null)
		{
			EntityPlayerSP player = Minecraft.getMinecraft().player;
			RenderLivingBase renderer = (RenderLivingBase) Minecraft.getMinecraft().getRenderManager().getEntityRenderObject(player);
			if (renderer != null && Minecraft.getMinecraft().gameSettings.thirdPersonView == 0)
			{
				ModelBiped model = (ModelBiped) renderer.getMainModel();
				model.setRotationAngles(0, 0, player.ticksExisted, 0f, 0f, 0.0625f, player);

				GlStateManager.pushMatrix();
				GlStateManager.translate(-0.2F, -0.28F, 0.1F);
				if (ContestHandler.pressedAmount > 1)
					GlStateManager.rotate(140.0F, 0.0F, 1.0F, 0.0F);
				else
					GlStateManager.rotate(150.0F, 0.0F, 1.0F, 0.0F);
				GlStateManager.rotate(80.0F, 1.0F, 0.0F, 0.0F);
				Minecraft.getMinecraft().renderEngine.bindTexture(player.getLocationSkin());
				model.bipedLeftArm.rotateAngleX *= 0.3;
				model.bipedLeftArm.rotateAngleY *= 0.3;
				model.bipedLeftArm.rotateAngleZ *= 0.3;
				model.bipedLeftArm.render(0.0625F);
				GlStateManager.popMatrix();

				GlStateManager.pushMatrix();
				GlStateManager.translate(0.05F, -0.3F, 0F);
				if (ContestHandler.pressedAmount > 1)
					GlStateManager.rotate(195.0F, 0.0F, 1.0F, 0.0F);
				else
					GlStateManager.rotate(185.0F, 0.0F, 1.0F, 0.0F);

				GlStateManager.rotate(80.0F, 1.0F, 0.0F, 0.0F);
				Minecraft.getMinecraft().renderEngine.bindTexture(player.getLocationSkin());
				model.bipedRightArm.rotateAngleX *= 0.3;
				model.bipedRightArm.rotateAngleY *= 0.3;
				model.bipedRightArm.rotateAngleZ *= 0.3;
				model.bipedRightArm.render(0.0625F);
				GlStateManager.popMatrix();

				event.setCanceled(true);

			}
		}
	}

	@SubscribeEvent(receiveCanceled = true)
	public void onSetupModel(RenderModelEvent.SetRotationAngels event)
	{
		IAdvancedCombatCapability cap = event.getEntity().getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
		if (cap == null)
			return;

		if (cap.isCombatModeEnabled())
		{
			Ability[] list = cap.getCombatBarAbilities();
			for (int i = 0; i < list.length; i++)
			{
				if (list[i] != null)
					list[i].onSetupModelInCombatBar(event, i);
			}
		}

		if (cap.getGrabee() != null)
		{
			event.setCanceled(true);
			event.model.bipedLeftArm.rotateAngleX = (float) Math.toRadians(-100);
			event.model.bipedLeftArm.rotateAngleY = (float) Math.toRadians(20);
			event.model.bipedLeftArm.rotateAngleZ = 0;
			if (event.model instanceof ModelPlayer)
			{
				((ModelPlayer) event.model).bipedLeftArmwear.rotateAngleX = (float) Math.toRadians(-100);
				((ModelPlayer) event.model).bipedLeftArmwear.rotateAngleY = (float) Math.toRadians(20);
				((ModelPlayer) event.model).bipedLeftArmwear.rotateAngleZ = 0f;
			}
			event.model.bipedRightArm.rotateAngleX = (float) Math.toRadians(-100);
			event.model.bipedRightArm.rotateAngleY = (float) Math.toRadians(-20);
			event.model.bipedRightArm.rotateAngleZ = 0;
			if (event.model instanceof ModelPlayer)
			{
				((ModelPlayer) event.model).bipedRightArmwear.rotateAngleX = (float) Math.toRadians(-100);
				((ModelPlayer) event.model).bipedRightArmwear.rotateAngleY = (float) Math.toRadians(-20);
				((ModelPlayer) event.model).bipedRightArmwear.rotateAngleZ = 0;

			}
		}
	}

	@SubscribeEvent
	public void onPlayerUpdate(LivingEvent.LivingUpdateEvent event)
	{
		if (event.getEntity() == mc.player)
		{
			IAdvancedCombatCapability capability = mc.player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);
			if (capability == null)
				return;
			if (capability.getGrabber() != null)
			{
				mc.player.motionX = 0;
				mc.player.motionY = 0;
				mc.player.motionZ = 0;

				mc.player.posX = capability.getGrabber().posX;
				mc.player.posY = capability.getGrabber().posY;
				mc.player.posZ = capability.getGrabber().posZ;
			}
			ContestHandler.onClientUpdate(capability);
		}
	}

	public static ResourceLocation HUD_TEX = new ResourceLocation(LucraftCore.MODID, "textures/gui/ability_bar.png");
	public static Minecraft mc = Minecraft.getMinecraft();

	@SubscribeEvent
	public void onRenderGameOverlay(RenderGameOverlayEvent e)
	{
		IAbilityBarEntry[] combatBarEntries = new IAbilityBarEntry[4];
		IAdvancedCombatCapability cap = mc.player.getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);

		if (cap.isCombatModeEnabled() && e.getType() == RenderGameOverlayEvent.ElementType.HOTBAR)
		{
			e.setCanceled(true);
		}

		if (e.isCancelable() || e.getType() != RenderGameOverlayEvent.ElementType.EXPERIENCE || mc.gameSettings.showDebugInfo || !cap
				.isCombatModeEnabled())
		{

			return;
		}

		Ability[] combatBarAbilities = cap.getCombatBarAbilities();

		for (int i = 0; i < combatBarAbilities.length; i++)
		{
			if (combatBarAbilities[i] != null)
				combatBarEntries[i] = new SuperpowerAbilityBarEntry(combatBarAbilities[i], i);
		}

		Tessellator tes = Tessellator.getInstance();
		BufferBuilder bb = tes.getBuffer();
		boolean names = mc.ingameGUI.getChatGUI().getChatOpen();
		ScaledResolution res = e.getResolution();

		int x = res.getScaledWidth() / 2 - 44;

		GlStateManager.pushMatrix();

		GlStateManager.enableRescaleNormal();
		GlStateManager.enableBlend();
		GlStateManager
				.tryBlendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE,
						GlStateManager.DestFactor.ZERO);
		RenderHelper.enableGUIStandardItemLighting();

		int k = 0;
		for (IAbilityBarEntry a : combatBarEntries)
		{
			if(a == null){
				k++;
				continue;
			}
			mc.renderEngine.bindTexture(HUD_TEX);
			mc.ingameGUI.drawTexturedModalRect(x, 0, 0, 0, 22, 22);
			GameSettings gs = Minecraft.getMinecraft().gameSettings;

			KeyBinding key = k == 0 ? gs.keyBindAttack : k == 1 ? gs.keyBindUseItem : k == 2 ? gs.keyBindDrop : gs.keyBindInventory;
			k++;
			String infoString = names ? a.getDescription() : GameSettings.getKeyDisplayString(key.getKeyCode());

			GlStateManager.pushMatrix();
			GlStateManager.disableTexture2D();
			GlStateManager.disableCull();
			mc.renderEngine.bindTexture(SuperpowerRenderLayer.WHITE_TEX);
			GlStateManager.color(0.2F, 0.2F, 0.2F, 0.5F);
			int infoHeight = (mc.fontRenderer.FONT_HEIGHT) * infoString.length();

			if (!names && (infoString.equals("Left Click") || infoString.equals("Right Click") || infoString.equals("Middle Click")))
			{
				infoHeight = mc.fontRenderer.FONT_HEIGHT;
			}

			bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
			bb.pos(x + 6, 22, 0).endVertex();
			bb.pos(x + 6, 24 + infoHeight + 6, 0).endVertex();
			bb.pos(x + 17, 24 + infoHeight + 6, 0).endVertex();
			bb.pos(x + 17, 22, 0).endVertex();
			tes.draw();

			if (a.renderCooldown())
			{
				Vec3d color = a.getCooldownColor();
				float percentage = a.getCooldownPercentage();

				GlStateManager.color(0, 0, 0);
				bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
				bb.pos(x + 4, 16, 50).endVertex();
				bb.pos(x + 4 + 14, 16, 50).endVertex();
				bb.pos(x + 4 + 14, 18, 50).endVertex();
				bb.pos(x + 4, 18, 50).endVertex();
				tes.draw();

				GlStateManager.color((float) color.x, (float) color.y, (float) color.z, 1);
				bb.begin(GL11.GL_POLYGON, DefaultVertexFormats.POSITION);
				bb.pos(x + 4, 16, 50).endVertex();
				bb.pos(x + 4 + percentage * 14, 16, 50).endVertex();
				bb.pos(x + 4 + percentage * 14, 17, 50).endVertex();
				bb.pos(x + 4, 17, 50).endVertex();
				tes.draw();
			}

			GlStateManager.enableTexture2D();
			GlStateManager.popMatrix();
			if (!names && infoString.equals("Left Click"))
			{
				mc.renderEngine.bindTexture(HUD_TEX);
				GlStateManager.pushMatrix();
				GlStateManager.translate(0, 0, 100);
				GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
				mc.ingameGUI.drawTexturedModalRect(x + 7, 24, 22, 9, 9, 14);
				infoString = "-";
				GlStateManager.popMatrix();
			}
			else if (!names && infoString.equals("Right Click"))
			{
				mc.renderEngine.bindTexture(HUD_TEX);
				GlStateManager.pushMatrix();
				GlStateManager.translate(0, 0, 100);
				GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
				mc.ingameGUI.drawTexturedModalRect(x + 7, 24, 31, 9, 9, 14);
				infoString = "-";
				GlStateManager.popMatrix();
			}
			else if (!names && infoString.equals("Middle Click"))
			{
				mc.renderEngine.bindTexture(HUD_TEX);
				GlStateManager.pushMatrix();
				GlStateManager.translate(0, 0, 100);
				GlStateManager.color(1.0f, 1.0f, 1.0f, 1.0f);
				mc.ingameGUI.drawTexturedModalRect(x + 7, 24, 40, 9, 9, 14);
				infoString = "-";
				GlStateManager.popMatrix();
			}

			GlStateManager.translate(0, 0, 50);
			GlStateManager.color(1, 1, 1, 1);
			a.drawIcon(mc, mc.ingameGUI, x + 3, 3);
			GlStateManager.translate(0, 0, -50);

			if (!infoString.equals("-"))
				for (int i = 0; i < infoString.toCharArray().length; i++)
				{
					mc.ingameGUI.drawString(mc.fontRenderer, String.valueOf(infoString.toCharArray()[i]), x + 9, 27 + i * 10, 0xfefefe);
				}

			x += 21;
		}

		RenderHelper.disableStandardItemLighting();
		GlStateManager.disableRescaleNormal();
		GlStateManager.disableBlend();
		GlStateManager.popMatrix();

	}
}
