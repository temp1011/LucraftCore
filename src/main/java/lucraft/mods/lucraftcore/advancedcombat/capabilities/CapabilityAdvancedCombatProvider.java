package lucraft.mods.lucraftcore.advancedcombat.capabilities;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;

public class CapabilityAdvancedCombatProvider implements ICapabilitySerializable<NBTTagCompound> {

	public IAdvancedCombatCapability instance;

	public CapabilityAdvancedCombatProvider(IAdvancedCombatCapability instance) {
		this.instance = instance;
	}

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
		return CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP != null && capability == CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP;
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
		return capability == CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP ? CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP.cast(instance) : null;
	}

	@Override
	public NBTTagCompound serializeNBT() {
		return (NBTTagCompound) CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP.getStorage().writeNBT(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, instance, null);
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt) {
		CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP.getStorage().readNBT(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, instance, null, nbt);
	}

}
