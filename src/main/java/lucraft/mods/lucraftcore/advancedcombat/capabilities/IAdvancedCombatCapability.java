package lucraft.mods.lucraftcore.advancedcombat.capabilities;

import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;

public interface IAdvancedCombatCapability {

	EntityPlayer getPlayer();

	NBTTagCompound writeNBT();

	void readNBT(NBTTagCompound nbt);

	void syncToPlayer();

	void syncToPlayer(EntityPlayer receiver);

	void syncToAll();

	void setCombatModeEnabled(boolean enabled);

	boolean isCombatModeEnabled();

	int getSelectedSlot();

	void setSelectedSlot(int currentItem);

	void scrollUpCombatBar(int index);

	void scrollDownCombatBar(int index);

	Ability[] getCombatBarAbilities();

	EntityLivingBase getGrabber();

	EntityLivingBase getGrabee();

	void setGrabber(EntityLivingBase entity);

	void setGrabee(EntityLivingBase entity);

	void resetCombatTimer();

	int getCombatTimer();

	void tickDownCombatTimer();
}
