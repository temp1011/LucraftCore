package lucraft.mods.lucraftcore.advancedcombat.capabilities;

import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.advancedcombat.ContestHandler;
import lucraft.mods.lucraftcore.advancedcombat.network.MessageSyncAdvancedCombat;
import lucraft.mods.lucraftcore.extendedinventory.InventoryExtendedInventory;
import lucraft.mods.lucraftcore.extendedinventory.capabilities.CapabilityExtendedInventory;
import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityHeld;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityToggle;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumHandSide;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.WorldServer;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.List;

public class CapabilityAdvancedCombat implements IAdvancedCombatCapability
{

	@CapabilityInject(IAdvancedCombatCapability.class)
	public static final Capability<IAdvancedCombatCapability> ADVANCED_COMBAT_CAP = null;

	public EntityPlayer player;

	private boolean combatModeEnabled;

	private int selectedSlot = -1;

	private String[] combatBarAbilities = new String[4];

	private EntityLivingBase grabber;
	private EntityLivingBase grabee;

	private int combatTimer;

	public CapabilityAdvancedCombat(EntityPlayer player)
	{
		this.player = player;
	}

	@Override
	public EntityPlayer getPlayer()
	{
		return this.player;
	}

	@Override
	public NBTTagCompound writeNBT()
	{
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setBoolean("combat_mode", this.combatModeEnabled);

		for (int i = 0; i < combatBarAbilities.length; i++)
		{
			nbt.setString("ability" + i, combatBarAbilities[i] == null || combatBarAbilities[i].isEmpty() ? "NO_ABILITY" : combatBarAbilities[i]);
		}
		if (grabee != null)
			nbt.setInteger("grabbedEntity", grabee.getEntityId());
		else
			nbt.setInteger("grabbedEntity", -1);

		if (grabber != null)
			nbt.setInteger("grabbingEntity", grabber.getEntityId());
		else
			nbt.setInteger("grabbingEntity", -1);

		nbt.setInteger("combatTimer", combatTimer);
		return nbt;
	}

	@Override
	public void readNBT(NBTTagCompound nbt)
	{
		this.combatModeEnabled = nbt.getBoolean("combat_mode");
		for (int i = 0; i < combatBarAbilities.length; i++)
		{
			combatBarAbilities[i] = nbt.getString("ability" + i);
		}
		int grabbedEntity = nbt.getInteger("grabbedEntity");
		Entity grabbed = player.world.getEntityByID(grabbedEntity);
		if (grabbed != null && grabbed instanceof EntityLivingBase)
		{
			this.grabee = (EntityLivingBase) grabbed;
		}
		else
			this.grabee = null;

		int grabbingEntity = nbt.getInteger("grabbingEntity");
		Entity grabbing = player.world.getEntityByID(grabbingEntity);
		if (grabbing != null && grabbing instanceof EntityLivingBase)
		{
			this.grabber = (EntityLivingBase) grabbing;
		}
		else
			this.grabber = null;
		this.combatTimer = nbt.getInteger("combatTimer");
	}

	@Override
	public void syncToPlayer()
	{
		this.syncToPlayer(this.player);
	}

	@Override
	public void syncToPlayer(EntityPlayer receiver)
	{
		if (receiver instanceof EntityPlayerMP)
			LCPacketDispatcher.sendTo(new MessageSyncAdvancedCombat(this.player), (EntityPlayerMP) receiver);
	}

	@Override
	public void syncToAll()
	{
		this.syncToPlayer();
		if (player.world instanceof WorldServer)
		{
			for (EntityPlayer players : ((WorldServer) player.world).getEntityTracker().getTrackingPlayers(player))
			{
				if (players instanceof EntityPlayerMP)
				{
					LCPacketDispatcher.sendTo(new MessageSyncAdvancedCombat(this.player), (EntityPlayerMP) players);
				}
			}
		}
	}

	@Override public int getSelectedSlot()
	{
		return selectedSlot;
	}

	@Override public void setSelectedSlot(int currentItem)
	{
		this.selectedSlot = currentItem;
	}

	@Override public void scrollUpCombatBar(int index)
	{
		Ability a = getCombatBarAbilities()[index];
		List<Ability> playerAbilities = Ability.getCurrentPlayerAbilities(player);
		playerAbilities.removeIf(ability -> !(ability.isUnlocked() && ability.showInAbilityBar() && !ability.alwaysHidden()));

		if (playerAbilities.isEmpty())
		{
			combatBarAbilities[index] = null;
			return;
		}

		int aIndex = a == null ? -1 : playerAbilities.indexOf(a);
		int i = aIndex;
		boolean unique;
		while (true)
		{
			i++;
			if (i > playerAbilities.size() - 1)
			{
				i = 0;
			}
			unique = true;
			for (Ability ability : getCombatBarAbilities())
			{
				if (ability != null && ability.getJsonID().equals(playerAbilities.get(i).getJsonID()))
				{
					unique = false;
				}
			}
			if (i == aIndex || unique)
				break;
		}
		combatBarAbilities[index] = playerAbilities.get(i).getJsonID();

	}

	@Override public void scrollDownCombatBar(int index)
	{
		Ability a = getCombatBarAbilities()[index];
		List<Ability> playerAbilities = Ability.getCurrentPlayerAbilities(player);
		playerAbilities.removeIf(ability -> !(ability.isUnlocked() && ability.showInAbilityBar() && !ability.alwaysHidden()));

		if (playerAbilities.isEmpty())
		{
			combatBarAbilities[index] = null;
			return;
		}

		int aIndex = a == null ? -1 : playerAbilities.indexOf(a);
		int i = aIndex;
		boolean unique;
		while (true)
		{
			i--;
			if (i < 0)
			{
				i = playerAbilities.size() - 1;
			}
			unique = true;
			for (Ability ability : getCombatBarAbilities())
			{
				if (ability != null && ability.getJsonID().equals(playerAbilities.get(i).getJsonID()))
				{
					unique = false;
				}
			}
			if (i == aIndex || unique)
				break;
		}
		combatBarAbilities[index] = playerAbilities.get(i).getJsonID();
	}

	@Override public Ability[] getCombatBarAbilities()
	{
		Ability[] returning = new Ability[4];
		for (int i = 0; i < combatBarAbilities.length; i++)
		{
			if (combatBarAbilities[i].isEmpty() || combatBarAbilities[i].equals("NO_ABILITY"))
			{
				if(i == 0) combatBarAbilities[0] = "lucraftcore:drawback_punch_left";
				if(i == 1) combatBarAbilities[1] = "lucraftcore:drawback_punch_right";
				if(i == 2) combatBarAbilities[2] = "lucraftcore:sweep_kick";
				if(i == 3) combatBarAbilities[3] = "lucraftcore:grab";
			}
		}

		ItemStack leftStack = player.getHeldItem(player.getPrimaryHand() == EnumHandSide.LEFT ? EnumHand.MAIN_HAND : EnumHand.OFF_HAND);
		ItemStack rightStack = player.getHeldItem(player.getPrimaryHand() == EnumHandSide.RIGHT ? EnumHand.MAIN_HAND : EnumHand.OFF_HAND);

		if(combatModeEnabled){
			if(leftStack.isEmpty()){
				if(combatBarAbilities[0].equals("lucraftcore:drawback_weapon_left"))
					combatBarAbilities[0] = "lucraftcore:drawback_punch_left";
			} else
				combatBarAbilities[0] = "lucraftcore:drawback_weapon_left";

			if(rightStack.isEmpty()){
				if(combatBarAbilities[1].equals("lucraftcore:drawback_weapon_right"))
					combatBarAbilities[1] = "lucraftcore:drawback_punch_right";
			} else
				combatBarAbilities[1] = "lucraftcore:drawback_weapon_right";
		}

		for (int i = 0; i < combatBarAbilities.length; i++)
		{
			for (Ability ability : Ability.getCurrentPlayerAbilities(player))
			{
				if (ability.getJsonID().equals(combatBarAbilities[i]))
					returning[i] = ability;
			}
		}



		return returning;
	}

	@Override public EntityLivingBase getGrabber()
	{
		return this.grabber;
	}

	@Override public EntityLivingBase getGrabee()
	{
		return this.grabee;
	}

	@Override public void setGrabber(EntityLivingBase entity)
	{
		this.grabber = entity;
	}

	@Override public void setGrabee(EntityLivingBase entity)
	{
		this.grabee = entity;
	}

	@Override public void resetCombatTimer()
	{
		this.combatTimer = 100;
	}

	@Override public int getCombatTimer()
	{
		return this.combatTimer;
	}

	@Override public void tickDownCombatTimer()
	{
		this.combatTimer--;
	}

	@Override public void setCombatModeEnabled(boolean enabled)
	{
		this.combatModeEnabled = enabled;
		InventoryExtendedInventory inv = player.getCapability(CapabilityExtendedInventory.EXTENDED_INVENTORY_CAP, null).getInventory();
		ItemStack left = inv.getStackInSlot(InventoryExtendedInventory.SLOT_LEFT_WEAPON);
		ItemStack right = inv.getStackInSlot(InventoryExtendedInventory.SLOT_RIGHT_WEAPON);
		if (player.getPrimaryHand() == EnumHandSide.LEFT)
		{
			ItemStack old = player.getHeldItem(EnumHand.MAIN_HAND);
			player.setHeldItem(EnumHand.MAIN_HAND, left);
			inv.setInventorySlotContents(InventoryExtendedInventory.SLOT_LEFT_WEAPON, old);

			old = player.getHeldItem(EnumHand.OFF_HAND);
			player.setHeldItem(EnumHand.OFF_HAND, right);
			inv.setInventorySlotContents(InventoryExtendedInventory.SLOT_RIGHT_WEAPON, old);
		}
		else
		{
			ItemStack old = player.getHeldItem(EnumHand.MAIN_HAND);
			player.setHeldItem(EnumHand.MAIN_HAND, right);
			inv.setInventorySlotContents(InventoryExtendedInventory.SLOT_RIGHT_WEAPON, old);

			old = player.getHeldItem(EnumHand.OFF_HAND);
			player.setHeldItem(EnumHand.OFF_HAND, left);
			inv.setInventorySlotContents(InventoryExtendedInventory.SLOT_LEFT_WEAPON, old);
		}

		for (Ability ability : getCombatBarAbilities())
		{
			if (ability instanceof AbilityToggle && ability.isEnabled())
			{
				ability.onKeyPressed();
			}
			else if (ability instanceof AbilityHeld && ability.isEnabled())
			{
				ability.onKeyReleased();
			}
		}
	}

	@Override public boolean isCombatModeEnabled()
	{
		return this.combatModeEnabled;
	}

	// ----------------------------------------------------------------------------------------------------------------------------

	public static class CapabilityAdvancedCombatEventHandler
	{

		@SubscribeEvent(priority = EventPriority.LOW)
		public void onAttachCapabilities(AttachCapabilitiesEvent<Entity> evt)
		{
			if (!(evt.getObject() instanceof EntityPlayer))
				return;

			evt.addCapability(new ResourceLocation(LucraftCore.MODID, "advanced_combat_capability"),
					new CapabilityAdvancedCombatProvider(new CapabilityAdvancedCombat((EntityPlayer) evt.getObject())));
		}

		@SubscribeEvent(priority = EventPriority.LOW)
		public void onPlayerStartTracking(PlayerEvent.StartTracking e)
		{
			if (e.getTarget().hasCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null))
			{
				e.getTarget().getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null).syncToPlayer(e.getEntityPlayer());
			}
		}

		@SubscribeEvent(priority = EventPriority.LOW)
		public void onEntityJoinWorld(EntityJoinWorldEvent e)
		{
			if (e.getEntity() instanceof EntityPlayer)
			{
				EntityPlayer player = (EntityPlayer) e.getEntity();
				IAdvancedCombatCapability data = player.getCapability(ADVANCED_COMBAT_CAP, null);
				data.syncToAll();
			}
		}

		@SubscribeEvent(priority = EventPriority.LOW)
		public void onPlayerClone(PlayerEvent.Clone e)
		{
			NBTTagCompound compound = (NBTTagCompound) ADVANCED_COMBAT_CAP.getStorage().writeNBT(
					ADVANCED_COMBAT_CAP, e.getOriginal().getCapability(
							ADVANCED_COMBAT_CAP, null), null);
			ADVANCED_COMBAT_CAP.getStorage().readNBT(
					ADVANCED_COMBAT_CAP, e.getEntityPlayer().getCapability(
							ADVANCED_COMBAT_CAP, null), null, compound);
		}

		@SubscribeEvent
		public void onUpdate(LivingEvent.LivingUpdateEvent event)
		{
			if (event.getEntity() instanceof EntityPlayer && event.getEntity().hasCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null))
			{
				IAdvancedCombatCapability cap = event.getEntity().getCapability(CapabilityAdvancedCombat.ADVANCED_COMBAT_CAP, null);

				cap.tickDownCombatTimer();

				if (cap.isCombatModeEnabled())
				{
					if (cap.getSelectedSlot() == -1)
					{
						cap.setSelectedSlot(((EntityPlayer) event.getEntity()).inventory.currentItem);
					}
					((EntityPlayer) event.getEntity()).inventory.currentItem = cap.getSelectedSlot();
				}
				else
				{
					if (cap.getSelectedSlot() != -1)
					{
						cap.setSelectedSlot(-1);
					}
				}
				if (!event.getEntity().world.isRemote)
				{
					for (ContestHandler.Contest contest : ContestHandler.contests)
					{
						if (contest.grabee == event.getEntityLiving() && cap.getGrabber() != null)
						{
							return;
						}
						if (contest.grabber == event.getEntityLiving() && cap.getGrabee() != null)
						{
							return;
						}
					}
					if (cap.getGrabee() != null)
					{
						cap.setGrabee(null);
						cap.syncToAll();
					}
					if (cap.getGrabber() != null)
					{
						cap.setGrabber(null);
						cap.syncToAll();
					}
				}
			}
		}
	}

	public static class CapabilityAdvancedCombatStorage implements IStorage<IAdvancedCombatCapability>
	{

		@Override
		public NBTBase writeNBT(Capability<IAdvancedCombatCapability> capability, IAdvancedCombatCapability instance, EnumFacing side)
		{
			return instance.writeNBT();
		}

		@Override
		public void readNBT(Capability<IAdvancedCombatCapability> capability, IAdvancedCombatCapability instance, EnumFacing side, NBTBase nbt)
		{
			instance.readNBT((NBTTagCompound) nbt);
		}

	}

}
