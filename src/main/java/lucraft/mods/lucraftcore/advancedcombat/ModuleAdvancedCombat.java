package lucraft.mods.lucraftcore.advancedcombat;

import lucraft.mods.lucraftcore.LCConfig;
import lucraft.mods.lucraftcore.LucraftCore;
import lucraft.mods.lucraftcore.advancedcombat.abilties.AbilityDrawbackPunch;
import lucraft.mods.lucraftcore.advancedcombat.abilties.AbilityDrawbackWeapon;
import lucraft.mods.lucraftcore.advancedcombat.abilties.AbilityGrab;
import lucraft.mods.lucraftcore.advancedcombat.abilties.AbilitySweepKick;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.CapabilityAdvancedCombat;
import lucraft.mods.lucraftcore.advancedcombat.capabilities.IAdvancedCombatCapability;
import lucraft.mods.lucraftcore.advancedcombat.keys.AdvancedCombatKeyBindings;
import lucraft.mods.lucraftcore.advancedcombat.network.*;
import lucraft.mods.lucraftcore.module.Module;
import lucraft.mods.lucraftcore.network.LCPacketDispatcher;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ModuleAdvancedCombat extends Module
{

	public static final ModuleAdvancedCombat INSTANCE = new ModuleAdvancedCombat();

	public static final int LEFT_HAND = 0, RIGHT_HAND = 1, DROP = 2, INV = 3;

	@Override
	public void preInit(FMLPreInitializationEvent event)
	{
		// Capability Registering
		CapabilityManager.INSTANCE.register(IAdvancedCombatCapability.class, new CapabilityAdvancedCombat.CapabilityAdvancedCombatStorage(), CapabilityAdvancedCombat.class);

		// EventHandler Registering
		MinecraftForge.EVENT_BUS.register(this);
		MinecraftForge.EVENT_BUS.register(new CapabilityAdvancedCombat.CapabilityAdvancedCombatEventHandler());
		MinecraftForge.EVENT_BUS.register(new AdvancedCombatEventHandler());

		//Packet Registering
		LCPacketDispatcher.registerMessage(MessageToggleCombat.Handler.class, MessageToggleCombat.class, Side.SERVER, 101);
		LCPacketDispatcher.registerMessage(MessageSyncAdvancedCombat.Handler.class, MessageSyncAdvancedCombat.class, Side.CLIENT, 102);
		LCPacketDispatcher.registerMessage(MessageScrollCombatAbilities.Handler.class, MessageScrollCombatAbilities.class, Side.SERVER, 103);
		LCPacketDispatcher.registerMessage(MessageRequestContestTotal.Handler.class, MessageRequestContestTotal.class, Side.CLIENT, 104);
		LCPacketDispatcher.registerMessage(MessageSendContestTotal.Handler.class, MessageSendContestTotal.class, Side.SERVER, 105);
	}

	@Override
	public void init(FMLInitializationEvent event) {
	}

	@Override
	public void postInit(FMLPostInitializationEvent event) {

	}

	@SubscribeEvent
	public void onRegisterAbilities(RegistryEvent.Register<Ability.AbilityEntry> e) {
		e.getRegistry().register(new Ability.AbilityEntry(AbilitySweepKick.class, new ResourceLocation(LucraftCore.MODID, "sweep_kick")));
		e.getRegistry().register(new Ability.AbilityEntry(AbilityDrawbackPunch.class, new ResourceLocation(LucraftCore.MODID, "drawback_punch")));
		e.getRegistry().register(new Ability.AbilityEntry(AbilityGrab.class, new ResourceLocation(LucraftCore.MODID, "grab")));
		e.getRegistry().register(new Ability.AbilityEntry(AbilityDrawbackWeapon.class, new ResourceLocation(LucraftCore.MODID, "drawback_weapon")));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void preInitClient(FMLPreInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(new AdvancedCombatKeyBindings());
		MinecraftForge.EVENT_BUS.register(new AdvancedCombatClientEventHandler());
	}

	@Override
	public String getName() {
		return "Advanced Combat";
	}

	@Override
	public boolean isEnabled() {
		return LCConfig.modules.advanced_combat;
	}

}
