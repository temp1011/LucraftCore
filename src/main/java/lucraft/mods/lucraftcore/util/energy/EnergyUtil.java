package lucraft.mods.lucraftcore.util.energy;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EnergyUtil {

    public static final String ENERGY_UNIT = "FE";

    @SideOnly(Side.CLIENT)
    public static void drawTooltip(EnergyStorage energyStorage, GuiContainer gui, int x, int y, int width, int height, int mouseX, int mouseY) {
        drawTooltip(energyStorage.getEnergyStored(), energyStorage.getMaxEnergyStored(), gui, x, y, width, height, mouseX, mouseY);
    }

    @SideOnly(Side.CLIENT)
    public static void drawTooltip(int energy, int maxEnergy, GuiContainer gui, int x, int y, int width, int height, int mouseX, int mouseY) {
        if(mouseX >= x && mouseX < x + width && mouseY >= y && mouseY < y + height) {
            gui.drawHoveringText(energy + "/" + maxEnergy + " " + ENERGY_UNIT, mouseX + 10, mouseY);
        }
    }

}
