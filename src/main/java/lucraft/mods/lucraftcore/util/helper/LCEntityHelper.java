package lucraft.mods.lucraftcore.util.helper;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

import java.util.UUID;

public class LCEntityHelper {
	public static boolean isInFrontOfEntity(Entity entity, Entity target) {
		Vec3d vec3d = target.getPositionVector();
		Vec3d vec3d1 = entity.getLook(1.0F);
		Vec3d vec3d2 = vec3d.subtractReverse(new Vec3d(entity.posX, entity.posY, entity.posZ)).normalize();
		vec3d2 = new Vec3d(vec3d2.x, 0.0D, vec3d2.z);

		return vec3d2.dotProduct(vec3d1) < 0.0D;
	}

	public static Entity getEntityByUUID(World world, UUID uuid) {
		for (Entity entity : world.loadedEntityList) {
			if (entity.getPersistentID().equals(uuid)) {
				return entity;
			}
		}

		return null;
	}

	public static Entity entityDropItem(EntityLivingBase entity, ItemStack stack, float offsetY, boolean removeFromInv) {
		if (stack.isEmpty()) {
			return null;
		} else {
			if(removeFromInv) {
				if(entity instanceof EntityPlayer) {
					EntityPlayer player = (EntityPlayer) entity;
					for(int i = 0; i < player.inventory.getSizeInventory(); i++) {
						if(player.inventory.getStackInSlot(i) == stack) {
							player.inventory.setInventorySlotContents(i, ItemStack.EMPTY);
							break;
						}
					}
				} else {
					for(EntityEquipmentSlot slots : EntityEquipmentSlot.values()) {
						if(entity.getItemStackFromSlot(slots) == stack) {
							entity.setItemStackToSlot(slots, ItemStack.EMPTY);
							break;
						}
					}
				}
			}

			EntityItem entityitem = new EntityItem(entity.world, entity.posX, entity.posY + (double) offsetY, entity.posZ, stack);
			entityitem.setDefaultPickupDelay();

			if (stack.getItem().hasCustomEntity(stack)) {
				Entity entityNew = stack.getItem().createEntity(entity.world, entityitem, stack);
				entity.world.spawnEntity(entityNew);
				return entityNew;
			}

			entity.world.spawnEntity(entityitem);
			return entityitem;
		}
	}

}
