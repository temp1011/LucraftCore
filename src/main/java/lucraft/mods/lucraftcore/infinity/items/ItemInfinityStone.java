package lucraft.mods.lucraftcore.infinity.items;

import lucraft.mods.lucraftcore.infinity.EnumInfinityStone;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.List;

public abstract class ItemInfinityStone extends ItemIndestructible {

    public abstract EnumInfinityStone getType();

    public abstract boolean isContainer();

    public int getEnergyPerTick(ItemStack stack) {
        return 100000;
    }

    @SideOnly(Side.CLIENT)
    public abstract List getAbilityBarEntries(EntityPlayer player, ItemStack stack);

    @Override
    public int getItemStackLimit(ItemStack stack) {
        return 1;
    }

    public void onGauntletTick(Entity entityIn, World world, ItemStack stack) {
    }

}