package lucraft.mods.lucraftcore.infinity.abilitybar;

import lucraft.mods.lucraftcore.infinity.ModuleInfinity;
import lucraft.mods.lucraftcore.infinity.items.InventoryInfinityGauntlet;
import lucraft.mods.lucraftcore.infinity.items.ItemInfinityStone;
import lucraft.mods.lucraftcore.util.abilitybar.IAbilityBarEntry;
import lucraft.mods.lucraftcore.util.abilitybar.IAbilityBarProvider;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class InfinityBarProvider implements IAbilityBarProvider {

    @Override
    public List<IAbilityBarEntry> getEntries() {
        EntityPlayer player = Minecraft.getMinecraft().player;
        ItemStack stack = player.getHeldItemMainhand();
        List<IAbilityBarEntry> entries = new ArrayList<>();

        if(!stack.isEmpty() && stack.hasTagCompound() && stack.getItem() == ModuleInfinity.INFINITY_GAUNTLET) {
            InventoryInfinityGauntlet inv = new InventoryInfinityGauntlet(stack);

            for(int i = 0; i < inv.getSizeInventory(); i++) {
                ItemStack s = inv.getStackInSlot(i);

                if(!s.isEmpty() && s.getItem() instanceof ItemInfinityStone) {
                    entries.addAll(((ItemInfinityStone)s.getItem()).getAbilityBarEntries(player, stack));
                }
            }
        }

        return entries;
    }

}
