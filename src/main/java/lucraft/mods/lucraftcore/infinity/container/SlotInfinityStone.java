package lucraft.mods.lucraftcore.infinity.container;

import lucraft.mods.lucraftcore.infinity.EnumInfinityStone;
import lucraft.mods.lucraftcore.infinity.items.ItemInfinityStone;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotInfinityStone extends Slot {

    public EnumInfinityStone type;

    public SlotInfinityStone(IInventory inventoryIn, EnumInfinityStone type, int index, int xPosition, int yPosition) {
        super(inventoryIn, index, xPosition, yPosition);
        this.type = type;
    }

    @Override
    public boolean isItemValid(ItemStack stack) {
        return stack.getItem() instanceof ItemInfinityStone && !((ItemInfinityStone)stack.getItem()).isContainer() && ((ItemInfinityStone)stack.getItem()).getType() == this.type;
    }
}
