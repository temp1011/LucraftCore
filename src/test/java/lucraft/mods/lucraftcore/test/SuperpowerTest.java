package lucraft.mods.lucraftcore.test;

import lucraft.mods.lucraftcore.superpowers.Superpower;
import lucraft.mods.lucraftcore.superpowers.SuperpowerPlayerHandler;
import lucraft.mods.lucraftcore.superpowers.abilities.*;
import lucraft.mods.lucraftcore.superpowers.capabilities.ISuperpowerCapability;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.UUID;

public class SuperpowerTest extends Superpower {

	public SuperpowerTest(String name) {
		super(name);
	}

	@Override
	protected java.util.List<lucraft.mods.lucraftcore.superpowers.abilities.Ability> addDefaultAbilities(net.minecraft.entity.player.EntityPlayer player, java.util.List<lucraft.mods.lucraftcore.superpowers.abilities.Ability> list) {
		list.add(new AbilityJumpBoost(player, UUID.fromString("2cf0af54-6129-473d-9a98-eddbbc35e490"), 5, 0).setUnlocked(true));
		list.add(new AbilityHealth(player, UUID.fromString("2cf0af54-6129-473d-9a98-eddbbc35e490"), 10, 0).setUnlocked(true));
		list.add(new AbilityFallResistance(player, UUID.fromString("2cf0af54-6129-473d-9a98-eddbbc35e490"), 2, 0).setUnlocked(true));
		list.add(new AbilityTeleport(player, 30, 5 * 20).setUnlocked(true));
		list.add(new AbilityFirePunch(player, 30, 5 * 20).setUnlocked(true));
		list.add(new AbilityPotionPunch(player, MobEffects.LEVITATION, 1, 100, 5 * 20).setUnlocked(true));
		list.add(new AbilitySlowfall(player).setUnlocked(true));
		list.add(new AbilityFlight(player).setUnlocked(true));
		list.add(new AbilityWaterBreathing(player).setUnlocked(true));
		list.add(new AbilityEnergyBlast(player, 5F, new Vec3d(1, 0, 0)).setMaxCooldown(5*20).setUnlocked(true));
		return list;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void renderIcon(net.minecraft.client.Minecraft mc, Gui gui, int x, int y) {
		GlStateManager.color(1, 1, 1);
		mc.renderEngine.bindTexture(TestMod.SUPERPOWER_ICON_TEX);
		gui.drawTexturedModalRect(x, y, 128, 0, 32, 32);
	}

	@Override
	public boolean canLevelUp() {
		return true;
	}

	@Override
	public int getMaxLevel() {
		return 10;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean canCustomize() {
		return true;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public lucraft.mods.lucraftcore.superpowers.gui.GuiCustomizer getCustomizerGui(EntityPlayer player) {
		return new GuiCustomizerTest();
	}

	@Override
	public net.minecraft.nbt.NBTTagCompound getDefaultStyleTag() {
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setFloat("Style", 0.5F);
		return nbt;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getCapsuleColor() {
		return 15073989;
	}

	@Override
	public SuperpowerPlayerHandler getNewSuperpowerHandler(ISuperpowerCapability cap) {
		return new SuperpowerPlayerHandler(cap, TestMod.TEST) {

			public int test;

			@Override
			public void onUpdate(Phase phase) {
				if (getPlayer() == null) {
					return;
				}

				if (phase == Phase.END && getPlayer().ticksExisted % 40 == 0) {
					// test++;
					// getPlayer().sendMessage(new
					// TextComponentString(cap.getCapabilityOwner().world.isRemote + ": " + test));
					// getPlayer().sendMessage(new TextComponentString("" +
					// getAbilities().toString()));
					// for(Ability ab : getAbilities())
					// ab.setUnlocked(true);
					// this.addXP(800);
				}
			};

			@Override
			public net.minecraft.nbt.NBTTagCompound writeToNBT(net.minecraft.nbt.NBTTagCompound compound) {
				super.writeToNBT(compound);
				compound.setInteger("TestInt", this.test);
				return compound;
			};

			@Override
			public void readFromNBT(net.minecraft.nbt.NBTTagCompound compound) {
				super.readFromNBT(compound);
				this.test = compound.getInteger("TestInt");
			};

		};
	}

}