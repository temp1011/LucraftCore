package lucraft.mods.lucraftcore.test;

import lucraft.mods.lucraftcore.superpowers.suitsets.ItemSuitSetArmor;
import lucraft.mods.lucraftcore.superpowers.suitsets.SuitSet;
import net.minecraft.inventory.EntityEquipmentSlot;

public class ItemSuitSetArmorTest extends ItemSuitSetArmor {

    public ItemSuitSetArmorTest(SuitSet suitSet, EntityEquipmentSlot slot) {
        super(suitSet, slot);
    }

}
