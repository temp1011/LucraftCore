package lucraft.mods.lucraftcore.test;

import java.io.IOException;

import lucraft.mods.lucraftcore.superpowers.SuperpowerHandler;
import lucraft.mods.lucraftcore.superpowers.gui.GuiCustomizer;
import lucraft.mods.lucraftcore.util.gui.GuiColorSlider;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.client.config.GuiButtonExt;
import net.minecraftforge.fml.client.config.GuiSlider;
import net.minecraftforge.fml.client.config.GuiSlider.ISlider;

public class GuiCustomizerTest extends GuiCustomizer implements ISlider {

	public float style;

	@Override
	public void initGui() {
		super.initGui();
		this.xSize = 256;
		this.ySize = 189;
		int i = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;
		NBTTagCompound old = SuperpowerHandler.getSuperpowerPlayerHandler(mc.player).getStyleNBTTag();
		this.style = old.getFloat("Style");

		this.buttonList.add(new GuiButtonExt(0, i + 4, j + 167, 50, 18, StringHelper.translateToLocal("lucraftcore.info.save")));
		this.buttonList.add(new GuiButtonExt(1, i + 202, j + 167, 50, 18, StringHelper.translateToLocal("gui.cancel")));

		this.buttonList.add(new GuiColorSlider(2, i + 20, j + 60, 80, 20, "Test", "", 0, 1, style, true, true, this));
	}

	@Override
	public NBTTagCompound getStyleNBTTag() {
		NBTTagCompound tag = new NBTTagCompound();
		tag.setFloat("Style", style);
		return tag;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		int i = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;

		if (SuperpowerHandler.getSuperpower(mc.player).getPlayerRenderer() != null)
			SuperpowerHandler.getSuperpower(mc.player).getPlayerRenderer().applyColor();

		mc.getTextureManager().bindTexture(GuiCustomizer.DEFAULT_TEX);
		this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize);

		this.drawString(mc.fontRenderer, StringHelper.translateToLocal("lucraftcore.info.customizer"), i + 5, j + 5, 0xffffff);
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		if (button.id == 0)
			sendStyleNBTTagToServer();
		if (button.id == 1)
			mc.player.closeScreen();
	}

	@Override
	public void onChangeSliderValue(GuiSlider slider) {
		this.style = (float) slider.sliderValue;
	}

}
